===============================================================================
ReadMe.txt

PLIN-API v2.8.0.154
Copyright (c) 2020 PEAK-System Technik GmbH Darmstadt, Germany
All rights reserved.
===============================================================================

Maximize the Notepad Window to read this file more easily.


Contents:
---------
  * Introduction
  * System Requirements
  * Rights to use these files
  * Contents of this directory
  * Installation
  * How to contact PEAK-System Technik GmbH


Introduction
------------
PLIN stands for PEAK LIN Applications and is a system for the development and
use of  LIN buses. It is a helpful and extensive product, directed to
developers and end-users.
The PLIN system allows the Real-time connection of Windows applications to the
LIN buses physically connected to the PC. 

The PLIN-API is a programming interface to the PLIN system. Via an interface DLL
it is possible to connect own applications to the device driver and PLIN devices
like the PLIN-USB adapter, to communicate with LIN buses.


Rights to use these files
-------------------------
PEAK-System Technik GmbH grants the right to the customer to use the files in
this software package as long as this is done in connection with original
hardware by PEAK-System or OEM hardware coming from PEAK-System. It is NOT
allowed to use any of these files (even not parts) with third-party hardware.

If you are not sure whether you have acquired an appropriate license with the
used hardware, please contact our technical support team (support@peak-system.com).


System Requirements
-------------------
- Operating systems: Windows 10/8.1/7
- A vacant USB port (USB 2.0 recommended) on the computer
- PLIN-USB adapter, PCAN-USB Pro FD adapter, or PCAN-USB Pro adapter


Contents of this directory
--------------------------
ReadMe.txt
    This text file.

LiesMich.txt
    The German translation of this file.

PLINAPI_enu.chm
    The PLIN-API documentation in HTML Help format, in English.

PLINAPI_enu.pdf
    The PLIN-API documentation in Acrobat Reader format, in English.

\Include
    Contains PLIN-API header files for different programming languages and
    development environments.

\Win32
    Contains the 32-bit interface DLL as well as a 32-bit demo application (exe).

    \BB_LIB
        32-bit LIB file for C++ Builder 

    \VC_LIB
        32-bit LIB file for Visual C/C++

\x64
    Contains the 64-bit interface DLL a 64-bit demo application (exe).

    \VC_LIB
        64-bit LIB file for Visual C/C++

\Samples
    Contains example files that demonstrate the use of the PLIN-API in
    different programming languages and development environments.

\Tools
    Contains tools that can be used for the development of PLIN applications.


Installation
------------
The PLIN-API is automatically deployed when the PLIN device driver is
installed. For information about the installation of PCAN hardware, please
refer to the user manual of the respective hardware. The hardware user manuals
are located in the folder <Product DVD>\Pdf.


How to contact PEAK-System Technik GmbH
---------------------------------------
If you have any questions concerning the installation of PCAN hardware, or
require information about other PEAK CAN products, then please contact us:

PEAK-System Technik GmbH
Otto-Roehm-Str. 69
D-64293 Darmstadt
Germany

Tel. +49 6151 / 8173-20
FAX  +49 6151 / 8173-29

support@peak-system.com
http://www.peak-system.com


LIFE SUPPORT APPLIANCES
-----------------------
These products are not designed for use in life support appliances, devices,
or systems where malfunction of these products can reasonably be expected to
result in personal injury. PEAK-System customers using or selling these
products for use in such applications do so at their own risk and agree to
fully indemnify PEAK-System for any damages resulting from such improper use
or sale.
