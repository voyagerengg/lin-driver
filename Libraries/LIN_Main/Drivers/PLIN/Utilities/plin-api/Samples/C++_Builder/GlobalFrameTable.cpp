#include <vcl.h>
#include "GlobalFrameTable.h"


//////////////////////////////////////////////////////////////////////////////////////////////
// TFrameDefinition class
//

TFrameDefinition::TFrameDefinition(TGlobalFrameTable *pParent, Byte id, Byte pid, int len,
	TLINChecksumType checksumType, TLINDirection direction)	:
	m_pParent(pParent),
	m_bID(id),
	m_bProtectedID(pid),
	m_nLength(len),
	m_checksumType(checksumType),
	m_direction(direction)
{
}

TFrameDefinition::~TFrameDefinition()
{
}

AnsiString TFrameDefinition::GetIdAsString()
{
	return AnsiString::Format("%.02Xh", ARRAYOFCONST((m_bID)));
}

AnsiString TFrameDefinition::GetProtectedIdAsString()
{
	return AnsiString::Format("%.02Xh", ARRAYOFCONST((m_bProtectedID)));
}

AnsiString TFrameDefinition::GetLengthAsString()
{
	return AnsiString::Format("%d", ARRAYOFCONST((m_nLength)));
}

void TFrameDefinition::SetLength(int length, bool doCallFramePropertyChanged /* = false */)
{
	bool allowed = true;
	if (m_pParent != NULL && doCallFramePropertyChanged)
		allowed = ((TGlobalFrameTable*) m_pParent)->CallFramePropertyChanged(propLength, this, (void *) length);
	if (allowed)
		m_nLength = length;
}

AnsiString TFrameDefinition::GetChecksumTypeAsString()
{
	return ChecksumTypeToString(m_checksumType);
}

AnsiString TFrameDefinition::GetDirectionAsString()
{
	return DirectionToString(m_direction);
}

void TFrameDefinition::SetChecksumType(TLINChecksumType checksumType, bool doCallFramePropertyChanged /* = false */)
{
	bool allowed = true;
	if (m_pParent != NULL && doCallFramePropertyChanged)
		allowed = ((TGlobalFrameTable*) m_pParent)->CallFramePropertyChanged(propChecksumType, this, (void *) checksumType);
	if (allowed)
		m_checksumType = checksumType;
}

void TFrameDefinition::SetDirection(TLINDirection direction, bool doCallFramePropertyChanged /* = false */)
{
	bool allowed = true;
	if (m_pParent != NULL && doCallFramePropertyChanged)
		allowed = ((TGlobalFrameTable*) m_pParent)->CallFramePropertyChanged(propDirection, this, (void *) direction);
	if (allowed)
		m_direction = direction;
}


//////////////////////////////////////////////////////////////////////////////////////////////
// TGlobalFrameTable class
//

TGlobalFrameTable::TGlobalFrameTable(PLinApiClass *pPLinApi) :
	m_pPLinApi(pPLinApi),
	m_hClient(0),
	m_hHw(0),
	m_HwMode(modNone),
	m_pMask(NULL)
{
	// Create all TFrameDefinition objects and set their values to default.
	// The length values is set to LIN 1.2.
	// Default values:
	// - Direction = SubscriberAutoLength
	// - ChecksumType = Auto
	// - Length = see inside the loop
	for (Byte id = 0; id < GLOBALFRAMETABLE_SIZE; id++)
	{
		// From Frame-ID 0 to 31 set the length 2
		if (id <= 0x1F)
			AddFrameDefinition(id, 2, cstAuto, dirSubscriberAutoLength);
		// From Frame-ID 32 to 47 set the length 4
		else if ((id >= 0x20) && (id <= 0x2F))
			AddFrameDefinition(id, 4, cstAuto, dirSubscriberAutoLength);
		// From Frame-ID 48 to 63 set the length 8
		else if ((id >= 0x30) && (id <= 0x3F))
			AddFrameDefinition(id, 8, cstAuto, dirSubscriberAutoLength);
	}
}

TGlobalFrameTable::~TGlobalFrameTable()
{
	// Delete all frames
	for (int i = 0; i < GLOBALFRAMETABLE_SIZE; i++)
		delete m_FrameDefinitions[i];
}

void TGlobalFrameTable::UpdateLinInfo(const HLINCLIENT hClient, const HLINHW hHw, const TLINHardwareMode hwMode, unsigned __int64 * pMask)
{
	// Up-to-date LIN information is required by function CallFramePropertyChanged
	m_hClient = hClient;
	m_hHw = hHw;
	m_HwMode = hwMode;
	m_pMask = pMask;
}

bool TGlobalFrameTable::CallFramePropertyChanged(
	EFRAMEPROPERTY propertyType,  // The property to modify
	TFrameDefinition *pFrame,     // Frame to update
	void* value)                  // The new value of the property
	// Return value determines whether the modificiation is allowed or not
{
	TLINFrameEntry frameEntry;
	TLINError err;
	unsigned __int64 mask;
	bool result = true;

	if (pFrame != NULL)
	{
		// If data length is to be set, check the value
		if (propertyType == propLength)
			// Only a values between 0 and 8 are valid
			result = ((int) value >= 0) && ((int) value <= 8);
		// If not allowed then return
		if (!result)
			return false;

		// Set the direction of the LIN-Frame only if the hardware was initialized as
		// Slave; the Master uses the direction with LIN_Write
		if (m_HwMode == modSlave)
		{
			// Set the Frame-ID of the frame to get and set.
			// The ID has to be set before getting the entry
			frameEntry.FrameId = pFrame->Id;
			// Get the frame entry with the Frame-ID from the hardware via the LIN-API
			err = m_pPLinApi->GetFrameEntry(m_hHw, &frameEntry);
			// If an error occurs do not allow to change the property and return
			if (err != errOK)
				return false;
			// Switch between the different kind of property types
			switch (propertyType)
			{
				// Direction property should be set
				case propDirection:
					frameEntry.Direction = (TLINDirection) value;
					break;
				// Length property should be set
				case propLength:
					frameEntry.Length = (Byte) value;;
					break;
				// ChecksumType property should be set
				case propChecksumType:
					frameEntry.ChecksumType = (TLINChecksumType) value;
					break;
			}
			frameEntry.Flags = FRAME_FLAG_RESPONSE_ENABLE;
			err = m_pPLinApi->SetFrameEntry(m_hClient, m_hHw, &frameEntry);
			// If an error occurs do not allow to change the property and return
			if (err != errOK)
				return false;
		}

		// If the property 'Direction' of one TFrameDefinition object is changed,
		// we need a special request to set the client filter here
		if (propertyType == propDirection)
		{
			// If the new value for the property 'Direction' is not 'Disabled', then
			// check if the TFrameDefinition is defined already with some
			// other value than 'Disabled'
			if ((TLINDirection)value != dirDisabled)
			{
				if (pFrame->Direction == dirDisabled)
				{
					// If the old direction of TFrameDefinition was set to 'Disabled', the
					// new value means that the Frame-ID has to be added to the client filter
					// via the LIN-API.
					// Set the client filter, the filter value is a bit mask.
					mask = (unsigned __int64)1 << pFrame->Id;
					*m_pMask = *m_pMask | mask;
					err = m_pPLinApi->SetClientFilter(m_hClient, m_hHw, *m_pMask);
					// Only allow to change the property if the Frame-ID has been successfully
					// added to the filter
					result = err == errOK;
				}
			}
			else
			{
				// If the direction is set to 'Disabled', remove the Frame-ID from the
				// client filter
				mask = (unsigned __int64)1 << pFrame->Id;
				*m_pMask = *m_pMask & ~mask;
				err = m_pPLinApi->SetClientFilter(m_hClient, m_hHw, *m_pMask);
				// Only allow to change the property if the Frame-ID has been removed
				// successfully from the filter
				result = err == errOK;
			}
		}
	}
	return result;
}

int TGlobalFrameTable::AddFrameDefinition(Byte id, int length, TLINChecksumType checksumType, TLINDirection direction)
{
	// Check the Frame-ID for before adding it, only ID's from 0 to 63 are allowed
	if (id > 63)
		return -1;	// ID is invalid, do not add it

	// The specified Frame-ID is valid.
	// Calculate the Protected-ID with the specified Frame-ID
	Byte bProtectedId = id;
	m_pPLinApi->GetPID(&bProtectedId);
	// Create a TFrameDefinition object and assign the specified values to it
	TFrameDefinition *pFrame = new TFrameDefinition(this, id, bProtectedId, length, checksumType, direction);
	// Add the created object to the list and return the position of the newly added object.
	// It should be added at the end so the position must be the last entry in the list.
	m_FrameDefinitions[id] = pFrame;

	return id;
}

TFrameDefinition * TGlobalFrameTable::GetFrames(int index)
{
	if (index >= 0 && index < GLOBALFRAMETABLE_SIZE)
		return m_FrameDefinitions[index];
	return NULL;
}

int TGlobalFrameTable::GetCount()
{
	return GLOBALFRAMETABLE_SIZE;
}

int TGlobalFrameTable::IndexOf(TFrameDefinition * pFrame)
{
	for (int i = 0; i < GLOBALFRAMETABLE_SIZE; i++)
		if (m_FrameDefinitions[i] == pFrame)
			return i;
	return -1;
}


//////////////////////////////////////////////////////////////////////////////////////////////
// Helper functions
//

AnsiString ChecksumTypeToString(TLINChecksumType checksumType)
{
	switch (checksumType)
	{
		case cstAuto:
			return "Auto";
		case cstClassic:
			return "Classic";
		case cstEnhanced:
			return "Enhanced";
		case cstCustom:
			return "Custom";
	}
	return EmptyStr;
}

AnsiString DirectionToString(TLINDirection direction)
{
	switch (direction)
	{
		case dirDisabled:
			return "Disabled";
		case dirPublisher:
			return "Publisher";
		case dirSubscriber:
			return "Subscriber";
		case dirSubscriberAutoLength:
			return "Subscriber Automatic Length";
	}
	return EmptyStr;
}

