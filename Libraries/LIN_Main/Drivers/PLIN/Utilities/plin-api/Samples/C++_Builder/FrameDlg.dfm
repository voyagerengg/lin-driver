object GlobalFrameTableDialog: TGlobalFrameTableDialog
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Global Frame Table'
  ClientHeight = 361
  ClientWidth = 743
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    743
    361)
  PixelsPerInch = 96
  TextHeight = 15
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 104
    Height = 15
    Caption = 'Global Frame Table:'
  end
  object Label2: TLabel
    Left = 452
    Top = 8
    Width = 56
    Height = 15
    Caption = 'Properties:'
  end
  object lvGFT: TListView
    Left = 8
    Top = 27
    Width = 435
    Height = 293
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'ID'
        Width = 45
      end
      item
        Caption = 'PID'
        Width = 45
      end
      item
        Caption = 'Direction'
        Width = 180
      end
      item
        Caption = 'Length'
      end
      item
        Caption = 'CST'
        Width = 90
      end>
    MultiSelect = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnKeyDown = lvGFTKeyDown
    OnSelectItem = lvGFTSelectItem
  end
  object pgGFTDef: TValueListEditor
    Left = 452
    Top = 27
    Width = 283
    Height = 293
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    OnStringsChange = pgGFTDefStringsChange
    ColWidths = (
      100
      177)
  end
  object btnClose: TButton
    Left = 660
    Top = 328
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 2
  end
end
