//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FrameDlg.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TGlobalFrameTableDialog *GlobalFrameTableDialog;
                                              
#define		PROP_CHECKSUMTYPE	"Checksum Type"
#define		PROP_DIRECTION		"Direction Type"
#define		PROP_LENGTH			"Length"
#define		PROP_ID				"ID"
#define		PROP_PID			"Protected ID"

//---------------------------------------------------------------------------
__fastcall TGlobalFrameTableDialog::TGlobalFrameTableDialog(TGlobalFrameTable* pGFT, TComponent* Owner)
	: TForm(Owner),
	m_pGFT(pGFT)
{
	m_pSelectedFrames = new TList();
	// Lock updates to prevent unnecessary events
	m_fAutomatic = true;
	__try
	{
		FillListView();
		int i, j;                                                       
		// Changeable properties : Checksum Type
		i = pgGFTDef->Strings->Add(AnsiString::Format("%s=", ARRAYOFCONST((PROP_CHECKSUMTYPE))));
		pgGFTDef->ItemProps[i]->EditStyle = esPickList;
		pgGFTDef->ItemProps[i]->ReadOnly = false;
		pgGFTDef->ItemProps[i]->PickList->AddObject(ChecksumTypeToString(cstAuto), (TObject*)cstAuto);
		pgGFTDef->ItemProps[i]->PickList->AddObject(ChecksumTypeToString(cstClassic), (TObject*)cstClassic);
		pgGFTDef->ItemProps[i]->PickList->AddObject(ChecksumTypeToString(cstEnhanced), (TObject*)cstEnhanced);
		pgGFTDef->ItemProps[i]->PickList->AddObject(ChecksumTypeToString(cstCustom), (TObject*)cstCustom);
		// Changeable properties: Direction
		i = pgGFTDef->Strings->Add(AnsiString::Format("%s=", ARRAYOFCONST((PROP_DIRECTION))));
		pgGFTDef->ItemProps[i]->EditStyle = esPickList;
		pgGFTDef->ItemProps[i]->ReadOnly = false;
		pgGFTDef->ItemProps[i]->PickList->AddObject(DirectionToString(dirDisabled), (TObject*)dirDisabled);
		pgGFTDef->ItemProps[i]->PickList->AddObject(DirectionToString(dirPublisher), (TObject*)dirPublisher);
		pgGFTDef->ItemProps[i]->PickList->AddObject(DirectionToString(dirSubscriber), (TObject*)dirSubscriber);
		pgGFTDef->ItemProps[i]->PickList->AddObject(DirectionToString(dirSubscriberAutoLength), (TObject*)dirSubscriberAutoLength);
		// Changeable properties: Length
		i = pgGFTDef->Strings->Add(AnsiString::Format("%s=", ARRAYOFCONST((PROP_LENGTH))));
		pgGFTDef->ItemProps[i]->EditStyle = esPickList;
		pgGFTDef->ItemProps[i]->ReadOnly = false;
		for (j = 1 ; j < 9 ; j++) {
			pgGFTDef->ItemProps[i]->PickList->AddObject(IntToStr(j), (TObject*)j);
		}
		// Read only properties: ID
		i = pgGFTDef->Strings->Add(AnsiString::Format("%s=", ARRAYOFCONST((PROP_ID))));
		pgGFTDef->ItemProps[i]->EditStyle = esSimple;
		pgGFTDef->ItemProps[i]->ReadOnly = true;
		// Read only properties: Protected ID
		i = pgGFTDef->Strings->Add(AnsiString::Format("%s=", ARRAYOFCONST((PROP_PID))));
		pgGFTDef->ItemProps[i]->EditStyle = esSimple;
		pgGFTDef->ItemProps[i]->ReadOnly = true;
	}
	__finally
	{
		// Remove lock
		m_fAutomatic = False;
	}
}

__fastcall TGlobalFrameTableDialog::~TGlobalFrameTableDialog()
{
	delete m_pSelectedFrames;
}

void TGlobalFrameTableDialog::FillListView()
{
	int i;
	TListItem* pCurrentItem;
	TFrameDefinition* pFrame;

	// Lock the ListView
	lvGFT->Items->BeginUpdate();
	__try
	{
		// Clear the ListView that will show the Frame Definition of the
		// Global Frame Table
		lvGFT->Items->Clear();
		// Add every object, Frame Definition, from the Global Frame Table
		// to the ListView
		for (i = 0; i < m_pGFT->Count; i++)
		{
			pFrame = m_pGFT->Frames[i];
			// Add the new ListView item with the type of the message
			pCurrentItem = lvGFT->Items->Add();
			pCurrentItem->Caption = pFrame->IdAsString;
			pCurrentItem->SubItems->Add(pFrame->ProtectedIdAsString);
			pCurrentItem->SubItems->Add(pFrame->DirectionAsString);
			pCurrentItem->SubItems->Add(pFrame->LengthAsString);
			pCurrentItem->SubItems->Add(pFrame->ChecksumTypeAsString);
		}
	}
	__finally
	{
	    // Unlock and update the ListView now
		lvGFT->Items->EndUpdate();
	}
}

void TGlobalFrameTableDialog::UpdatePropertyGrid()
{
	TFrameDefinition* pFrame;
	TLINDirection direction;
	TLINChecksumType checksumType;
	bool bSameDirection, bSameChecksumType, bSameLength;
	int len, i;

	if (m_pSelectedFrames->Count == 0)
	{
		// Reset "property grid" values
		pgGFTDef->Values[PROP_CHECKSUMTYPE] = EmptyStr;
		pgGFTDef->Values[PROP_DIRECTION] = EmptyStr;
		pgGFTDef->Values[PROP_LENGTH] = EmptyStr;
		pgGFTDef->Values[PROP_ID] = EmptyStr;
		pgGFTDef->Values[PROP_PID] = EmptyStr;
	}
	else if (m_pSelectedFrames->Count == 1)
	{
		// Fill "property grid" with selected frame values
		pFrame = (TFrameDefinition*) m_pSelectedFrames->Items[0];
		pgGFTDef->Values[PROP_CHECKSUMTYPE] = pFrame->ChecksumTypeAsString;
		pgGFTDef->Values[PROP_DIRECTION] = pFrame->DirectionAsString;
		pgGFTDef->Values[PROP_LENGTH] = pFrame->LengthAsString;
		pgGFTDef->Values[PROP_ID] = pFrame->IdAsString;
		pgGFTDef->Values[PROP_PID] = pFrame->ProtectedIdAsString;
	}
	else
	{
		// Get the first selected frame
		pFrame = (TFrameDefinition*) m_pSelectedFrames->Items[0];
		// Get its properties
		checksumType = pFrame->ChecksumType;
		direction = pFrame->Direction;
		len = pFrame->Length;
		// We have to loop through all selected frames to
		// search for identical properties in the selected frames.
		// If values for a property are different a blank value is displayed
		bSameDirection = true;
		bSameChecksumType = true;
		bSameLength = true;
		for (i = 0 ; i < m_pSelectedFrames->Count ; i++)
		{
			pFrame = (TFrameDefinition*)m_pSelectedFrames->Items[i];
			if (checksumType != pFrame->ChecksumType)
				bSameChecksumType = false;
			if (direction != pFrame->Direction)
				bSameDirection = false;
			if (len != pFrame->Length)
				bSameLength = false;
		}
		pFrame = (TFrameDefinition*)m_pSelectedFrames->Items[0];
		// If all the frames have the same CST, display it otherwise reset to the original blank value
		if (bSameChecksumType)
			pgGFTDef->Values[PROP_CHECKSUMTYPE] = pFrame->ChecksumTypeAsString;
		else
			pgGFTDef->Values[PROP_CHECKSUMTYPE] = EmptyStr;
		// If all the frames have the same direction, display it otherwise reset to the original blank value
		if (bSameDirection)
			pgGFTDef->Values[PROP_DIRECTION] = pFrame->DirectionAsString;
		else
			pgGFTDef->Values[PROP_DIRECTION] = EmptyStr;
		// If all the frames have the same length, display it otherwise reset to the original blank value
		if (bSameLength)
			pgGFTDef->Values[PROP_LENGTH] = pFrame->LengthAsString;
		else
			pgGFTDef->Values[PROP_LENGTH] = EmptyStr;
		// These properties are always different, reset to the original blank value
		pgGFTDef->Values[PROP_ID] = EmptyStr;
		pgGFTDef->Values[PROP_PID] = EmptyStr;
	}
}
//---------------------------------------------------------------------------
void __fastcall TGlobalFrameTableDialog::lvGFTKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	// Select all items
	if (Key == 'A' && Shift.Contains(ssCtrl))
	{
		((TListView*)Sender)->SelectAll();
		Key = 0;
	}
}
//---------------------------------------------------------------------------
void __fastcall TGlobalFrameTableDialog::lvGFTSelectItem(TObject *Sender, TListItem *Item, bool Selected)
{
	int nIdx;
	TListItem* curItem;
	TItemStates selected = TItemStates() << isSelected;

	// Avoid internal calls
	if (!m_fAutomatic)
	{
		m_fAutomatic = true;
		__try
		{
			// Clear the selected frames list
			m_pSelectedFrames->Clear();
			// Get the count of selected elements in the ListView 'lvGFT'.
			// Each element is associated with an element in the CGlobalFrameTable object
			curItem = lvGFT->Selected;
			if (curItem != NULL)
			{
				// Add all items to the internal item selection list
				nIdx = lvGFT->Items->IndexOf(curItem);
				m_pSelectedFrames->Add(m_pGFT->Frames[nIdx]);
				curItem = lvGFT->GetNextItem(curItem, sdAll, selected);
				// If there are more than one
				while (curItem != NULL)
				{
					nIdx = lvGFT->Items->IndexOf(curItem);
					m_pSelectedFrames->Add(m_pGFT->Frames[nIdx]);
					curItem = lvGFT->GetNextItem(curItem, sdAll, selected);
				}
			}
			// Update the PropertyGrid control with the newly selected frames
			UpdatePropertyGrid();
		}
		__finally
		{
	        // Remove lock
			m_fAutomatic = false;
		}
	}
}
//---------------------------------------------------------------------------
void __fastcall TGlobalFrameTableDialog::pgGFTDefStringsChange(TObject *Sender)
{
	AnsiString value;
	TLINDirection direction;
	TLINChecksumType checksumType;
	int length, i;
	TFrameDefinition* pFrame;
	TListItem* pCurrentItem;
	bool setLen, setCST, setDir;

	// Avoid internal calls
	if (!m_fAutomatic)
	{
		m_fAutomatic = true;
		__try
		{
			// If no frame is selected, only update propertyGrid to display original/blank values and return
			if (m_pSelectedFrames->Count == 0)
			{
				// Update and return
				UpdatePropertyGrid();
				return;
			}

			// Since the event do not state which row was changed the following booleans
			// define wether the values are valid or not (needed for multiple row selection)
			setDir = false;
			setCST = false;
			setLen = false;
			checksumType = cstAuto;
			direction = dirSubscriberAutoLength;
			length = 0;

			// Checksum property changed
			value = pgGFTDef->Values[PROP_CHECKSUMTYPE];
			if (value != EmptyStr)
			{
				setCST = true;
				if (value == ChecksumTypeToString(cstCustom))
					checksumType = cstCustom;
				else if (value == ChecksumTypeToString(cstClassic))
					checksumType = cstClassic;
				else if (value == ChecksumTypeToString(cstEnhanced))
					checksumType = cstEnhanced;
				else if (value == ChecksumTypeToString(cstAuto))
					checksumType = cstAuto;
			}

			// Direction property changed
			value = pgGFTDef->Values[PROP_DIRECTION];
			if (value != EmptyStr)
			{
				setDir = true;
				if (value == DirectionToString(dirDisabled))
					direction = dirDisabled;
				else if (value == DirectionToString(dirPublisher))
					direction = dirPublisher;
				else if (value == DirectionToString(dirSubscriber))
					direction = dirSubscriber;
				else if (value == DirectionToString(dirSubscriberAutoLength))
					direction = dirSubscriberAutoLength;
			}

			// Length property changed
			value = pgGFTDef->Values[PROP_LENGTH];
			if (value != EmptyStr)
			{
				if (TryStrToInt(value, length))
					setLen = true;
			}

			for (i = 0 ; i < m_pSelectedFrames->Count ; i++)
			{
				pFrame = (TFrameDefinition*) m_pSelectedFrames->Items[i];
				// Update CST : avoid unnecessary updates
				if (setCST && (pFrame->ChecksumType != checksumType))
				{
					// call for propertyChanged to update dialog controls
					pFrame->SetChecksumType(checksumType, true);
					// if changes were not validated, rollback/update display
					if (pFrame->ChecksumType != checksumType)
						UpdatePropertyGrid();
				}
				// Update Direction : avoid unnecessary updates
				if (setDir && (pFrame->Direction != direction))
				{
					// call for propertyChanged to update dialog controls
					pFrame->SetDirection(direction, true);
					// if changes were not validated, rollback/update display
					if (pFrame->Direction != direction)
						UpdatePropertyGrid();
				}
				// Update length : avoid unnecessary updates
				if (setLen && (pFrame->Length != length))
				{
					// call for propertyChanged to update dialog controls
					pFrame->SetLength(length, true);
					// if changes were not validated, rollback/update display
					if (pFrame->Length != length)
						UpdatePropertyGrid();
				}

				// Update the frame in the list view
				lvGFT->Items->BeginUpdate();
				__try
				{
					pCurrentItem = lvGFT->Items->Item[pFrame->Id];
					pCurrentItem->SubItems->Strings[0] = pFrame->ProtectedIdAsString;
					pCurrentItem->SubItems->Strings[1] = pFrame->DirectionAsString;
					pCurrentItem->SubItems->Strings[2] = pFrame->LengthAsString;
					pCurrentItem->SubItems->Strings[3] = pFrame->ChecksumTypeAsString;
				}
				__finally
				{
					lvGFT->Items->EndUpdate();
				}
			}
		}
		__finally
		{
			// Remove lock
			m_fAutomatic = false;
		}
	}
}
//---------------------------------------------------------------------------




