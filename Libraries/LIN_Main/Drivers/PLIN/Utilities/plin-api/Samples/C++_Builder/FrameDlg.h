//---------------------------------------------------------------------------

#ifndef FrameDlgH
#define FrameDlgH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
    
#include "GlobalFrameTable.h"
//---------------------------------------------------------------------------
class TGlobalFrameTableDialog : public TForm
{
__published:
	TLabel *Label1;
	TLabel *Label2;
	TListView *lvGFT;
	TValueListEditor *pgGFTDef;
	TButton *btnClose;
	void __fastcall lvGFTKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall lvGFTSelectItem(TObject *Sender, TListItem *Item,
		  bool Selected);
	void __fastcall pgGFTDefStringsChange(TObject *Sender);
private:
    // Global Frame Table object that holds all frame definition.
	TGlobalFrameTable * m_pGFT;
	/// Boolean to register the current edit mode of different controls.
	/// To protect the control's event for recursive calling.
	bool m_fAutomatic;
	// Stores the selected frames in the Frame List Control
	TList * m_pSelectedFrames;

	// Fills the Listview 'Global Frame Table' with the data from CGloabalFrameTable.
	void FillListView();
	// Updates the property grid with the selected frames from m_pSelectedFrames
	void UpdatePropertyGrid();

public:
	__fastcall TGlobalFrameTableDialog(TGlobalFrameTable* pGFT, TComponent* Owner);
	__fastcall ~TGlobalFrameTableDialog();
};
//---------------------------------------------------------------------------
extern PACKAGE TGlobalFrameTableDialog *GlobalFrameTableDialog;
//---------------------------------------------------------------------------
#endif
