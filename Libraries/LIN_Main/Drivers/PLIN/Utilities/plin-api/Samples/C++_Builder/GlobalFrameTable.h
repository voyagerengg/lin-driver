
// GlobalFrameTable.h : header file
//

#ifndef __CGLOBALFRAMETABLEH_
#define __CGLOBALFRAMETABLEH_

// Inclusion of the PLinApi.h header file
//
#ifndef __PLINAPICLASSH_
#include "PLinApiClass.h"
#endif

////////////////////////////////////////////////////////////
// Value definitions

// Size of the LIN Global Frame Table
#define GLOBALFRAMETABLE_SIZE	64

// Changeable LIN Frame properties
//
#define propDirection			1
#define propChecksumType		2
#define propLength				3
#define EFRAMEPROPERTY			WORD

class TGlobalFrameTable;

// LIN Frame Definition class for handling property changes and message communication
//
class TFrameDefinition
{
private:
	// The parent TGlobalFrameTable object containing this frame
	TGlobalFrameTable* m_pParent;
	// LIN-Frame Identifier
	Byte m_bID;
	// Data length of the LIN-Frame
	int m_nLength;
	// Type of the Checksum Calculation
	TLINChecksumType m_checksumType;
	// Direction of the LIN-Frame
	TLINDirection m_direction;
	// Protected LIN-Frame Identifier
	Byte m_bProtectedID;

	// Property getters
	AnsiString GetIdAsString();
	AnsiString GetProtectedIdAsString();
	AnsiString GetLengthAsString();
	AnsiString GetChecksumTypeAsString();
	AnsiString GetDirectionAsString();

public:
	// Constructor
	TFrameDefinition(
		TGlobalFrameTable *pParent,     // Parent TGlobalFrameTable object containing this frame
		Byte id,						// LIN-Frame ID
		Byte pid,						// LIN-Frame Protected ID
		int len,						// LIN-Frame length
		TLINChecksumType checksumType,	// LIN-Frame checksum type
		TLINDirection direction);		// LIN-Frame direction
	// Standard destructor
	~TFrameDefinition();

	// Returns the LIN-Frame identifier
	__property Byte Id = {read = m_bID};
	// Returns the LIN-Frame identifier as a string
	__property AnsiString IdAsString = {read = GetIdAsString};

	// Returns the protected LIN-Frame identifier
	__property Byte ProtectedId = {read = m_bProtectedID};
	// Returns the protected LIN-Frame identifier as a string
	__property AnsiString ProtectedIdAsString = {read = GetProtectedIdAsString};

	// Returns the LIN-Frame length
	__property int Length = {read = m_nLength};
	// Returns the LIN-Frame length as a string.
	__property AnsiString LengthAsString = {read = GetLengthAsString};

	// Returns the LIN-Frame checksumType
	__property TLINChecksumType ChecksumType = {read = m_checksumType};
	// Returns the LIN-Frame checksumType as a string
	__property AnsiString ChecksumTypeAsString = {read = GetChecksumTypeAsString};

	// Returns the LIN-Frame direction
	__property TLINDirection Direction = {read = m_direction};
	// Returns the LIN-Frame direction
	__property AnsiString DirectionAsString = {read = GetDirectionAsString};

	// Setters with calls to "CallFramePropertyChanged" parent function

	// Sets the LIN-Frame length
	void SetLength(int length, bool doCallFramePropertyChanged = false);
	// Sets the LIN-Frame checksumType
	void SetChecksumType(TLINChecksumType checksumType, bool doCallFramePropertyChanged = false);
	// Sets the LIN-Frame direction
	void SetDirection(TLINDirection direction, bool doCallFramePropertyChanged = false);
};


// LIN Global Frame Table class, contains and handles the 64 LIN Frames/CFrameDefinitions
//
class TGlobalFrameTable
{
	friend class TFrameDefinition;

private:	
    // LIN API object
	PLinApiClass *m_pPLinApi;

	// LIN Client handle
	HLINCLIENT m_hClient;
    // LIN Hardware handle
    HLINHW m_hHw;
    // LIN Hardware Modus
	TLINHardwareMode m_HwMode;
    // pointer to LIN Client filter mask
	unsigned __int64 * m_pMask;

	// Collection of all frame definitions
	TFrameDefinition * m_FrameDefinitions[GLOBALFRAMETABLE_SIZE];

	// Getter for Count property
	int GetCount();

	// Getter for Frames property
	TFrameDefinition* GetFrames(int index);

public:
	TGlobalFrameTable(PLinApiClass * objPLinApi);
	~TGlobalFrameTable();

	// Returns the index in the table for the specified Frame object
	int IndexOf(TFrameDefinition *pFrame);

	// Sets internal LIN informations
	void UpdateLinInfo(const HLINCLIENT hClient, const HLINHW hHw, const TLINHardwareMode hwMode, unsigned __int64 * pMask);

	// Returns the size of the Frame Table
	__property int Count = {read = GetCount};

	// Returns the TFrameDefinition object based on the specified index (with 0 <= index < 64)
	__property TFrameDefinition* Frames[int Index] = {read = GetFrames};

private:
	// Function is called when a property of TFrameDefinition is changed to assert the modification is allowed.
	// Return value defines if the modification is allowed or not.
	bool CallFramePropertyChanged(
		EFRAMEPROPERTY propertyType,	// The property to modify
		TFrameDefinition *pFrame,		// Frame to update
		void* value);					// The new value of the property

	// Adds a new frame definition to the global frame table.
	// Returns the frame index in the table (the same as id unless an error occurred).
	int AddFrameDefinition(
		Byte id,	       	            // Position (and ID) of the frame in the table
		int length,		                // Length
		TLINChecksumType checksumType,	// Checksum type
		TLINDirection direction);		// Direction

};

////////////////////////////////////////////////////////////
// Helper functions

// Converts a LIN Frame checksum type into a human-readable string
AnsiString ChecksumTypeToString(TLINChecksumType checksumType);

// Converts a LIN Frame Direction into a human-readable string
AnsiString DirectionToString(TLINDirection direction);

#endif
