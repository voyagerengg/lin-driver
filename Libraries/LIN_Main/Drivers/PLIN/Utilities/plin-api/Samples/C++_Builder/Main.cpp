//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "FrameDlg.h"
#include <math.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#define    	SLinCSTAuto             "Auto"
#define     SLinCSTClassic          "Classic"
#define     SLinCSTCustom           "Custom"
#define     SLinCSTEnhanced         "Enhanced"
#define     SLinDirectionAuto       "Subscriber Automatic Length"
#define     SLinDirectionDisabled   "Disabled"
#define     SLinDirectionPublisher  "Publisher"
#define     SLinDirectionSubscriber "Subscriber"
#define     SWriteAsMaster          "Transmit"
#define     SWriteAsSlave           "Publish"

//////////////////////////////////////////////////////////////////////////////////////////////
// CMessageStatus class
//

TMessageStatus::TMessageStatus(TLINRcvMsg linMsg, int listIndex) :
	m_msg(linMsg),
	m_oldTimeStamp(linMsg.TimeStamp),
	m_iIndex(listIndex),
	m_nCount(1),
	m_bShowPeriod(true),
	m_bWasChanged(false)
{
}

void TMessageStatus::Update(TLINRcvMsg linMsg)
{
	m_oldTimeStamp = m_msg.TimeStamp;
	m_msg = linMsg;
	m_nCount += 1;
	m_bWasChanged = true;
}

AnsiString TMessageStatus::GetDirectionString()
{
	return DirectionToString(m_msg.Direction);
}

AnsiString TMessageStatus::GetCSTString()
{
	return ChecksumTypeToString(m_msg.ChecksumType);
}

AnsiString TMessageStatus::GetChecksumString()
{
	return AnsiString::Format("%Xh", ARRAYOFCONST((m_msg.Checksum)));
}

AnsiString TMessageStatus::GetErrorString()
{
	if (m_msg.ErrorFlags == 0)
		return "O.k.";

	AnsiString result;
	if (m_msg.ErrorFlags & MSG_ERR_CHECKSUM)
		result += "Checksum,";
	if (m_msg.ErrorFlags & MSG_ERR_GND_SHORT)
		result += "GroundShort,";
	if (m_msg.ErrorFlags & MSG_ERR_ID_PARITY_BIT0)
		result += "IdParityBit0,";
	if (m_msg.ErrorFlags & MSG_ERR_ID_PARITY_BIT1)
		result += "IdParityBit1,";
	if (m_msg.ErrorFlags & MSG_ERR_INCONSISTENT_SYNC)
		result += "InconsistentSynch,";
	if (m_msg.ErrorFlags & MSG_ERR_OTHER_RESPONSE)
		result += "OtherResponse,";
	if (m_msg.ErrorFlags & MSG_ERR_SLAVE_NOT_RESPONDING)
		result += "SlaveNotResponding,";
	if (m_msg.ErrorFlags & MSG_ERR_SLOT_DELAY)
		result += "SlotDelay,";
	if (m_msg.ErrorFlags & MSG_ERR_TIMEOUT)
		result += "Timeout,";
	if (m_msg.ErrorFlags & MSG_ERR_VBAT_SHORT)
		result += "VBatShort,";
	// Remove trailing comma
	if (result.Length() > 0)
		result.Delete(result.Length()-1, 1);
	return result;
}

AnsiString TMessageStatus::GetProtectedIdString()
{
	return AnsiString::Format("%.03Xh", ARRAYOFCONST((m_msg.FrameId)));
}

AnsiString TMessageStatus::GetDataString()
{
	AnsiString result;
	for (int i = 0; i < (int) m_msg.Length; i++)
		result += AnsiString::Format("%.02X ", ARRAYOFCONST((m_msg.Data[i])));
	return result;
}

void TMessageStatus::SetShowPeriod(bool value)
{
	if (value != m_bShowPeriod)
	{
		m_bShowPeriod = value;
		m_bWasChanged = true;
	}
}
AnsiString TMessageStatus::GetTimeString()
{
	AnsiString result;
	char buffer[65];
	unsigned __int64 time;

	memset(buffer, '\0', 64);
	time = m_msg.TimeStamp;
	if (m_bShowPeriod)
		time = (time - m_oldTimeStamp) / 1000;
	_ui64toa(time, buffer, 10);
	result = Format("%s", ARRAYOFCONST((buffer)));
    
	return result;
}
    
//////////////////////////////////////////////////////////////////////////////////////////////
// TMainForm class
//

TMainForm *MainForm;

//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormCreate(TObject *Sender)
{
	InitializeControls();

	// Create a PLinApi class instance
	m_pPLinApi = new PLinApiClass();
	// Create Global Frame table (LIN information are set upon connection)
	m_pGFT = new TGlobalFrameTable(m_pPLinApi);
	// Populates FrameID combobox with global frame IDs
	UpdateFrameIds();

	// Initialize the LIN attributes
	m_hClient = 0;
	m_hHw = 0;
	m_lMask = FRAME_FILTER_MASK;
	m_HwMode = modSlave;
	m_wBaudrate = 19200;

	// Create a list to store the displayed messages
	m_LastMsgsList = new TList();
}
//---------------------------------------------------------------------------
void TMainForm::InitializeControls(void)
{
	// Populates Direction combobox
	cbbDirection->Items->Clear();
	cbbDirection->Items->AddObject(SLinDirectionDisabled, (TObject*)dirDisabled);
	cbbDirection->Items->AddObject(SLinDirectionPublisher, (TObject*)dirPublisher);
	cbbDirection->Items->AddObject(SLinDirectionSubscriber, (TObject*)dirSubscriber);
	cbbDirection->Items->AddObject(SLinDirectionAuto, (TObject*)dirSubscriberAutoLength);
	// Populates ChecksumType combobox
	cbbCST->Items->Clear();
	cbbCST->Items->AddObject(SLinCSTAuto, (TObject*)cstAuto);
	cbbCST->Items->AddObject(SLinCSTClassic, (TObject*)cstClassic);
	cbbCST->Items->AddObject(SLinCSTEnhanced, (TObject*)cstEnhanced);
	cbbCST->Items->AddObject(SLinCSTCustom, (TObject*)cstCustom);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
	// Free resources that have been initialized in FormCreate()
	delete m_pGFT;
	delete m_pPLinApi;
	for (int i = 0 ; i < m_LastMsgsList->Count ; i++)
		delete m_LastMsgsList->Items[i];
	delete m_LastMsgsList;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject *Sender)
{
	// Initialize default state of components
	btnHwRefreshClick(NULL);	// Refresh hardware list combobox
	rdbFilterChanged(NULL);
	cbbDirection->ItemIndex = 0;
	cbbCST->ItemIndex = 1;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	// Disconnect from LIN if necessary
	if (m_hClient != 0)
	{
		DoLinDisconnect();
		m_hHw = 0;
		// Unregister the application
		m_pPLinApi->RemoveClient(m_hClient);
		m_hClient = 0;
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////
//   Connection event-handlers

void __fastcall TMainForm::cbbChannelChange(TObject *Sender)
{
	// Get the handle from the combobox item
	HLINHW hHw = (HLINHW) cbbChannel->Items->Objects[cbbChannel->ItemIndex];
	if (hHw != 0)
	{
		int mode, baudrate;
		btnInit->Enabled = true;
		btnIdent->Enabled = true;
		// Read the mode of the hardware with the handle hHw (Master, Slave, or None)
		m_LastLINErr = m_pPLinApi->GetHardwareParam(hHw, hwpMode, &mode, 0);
		// Read the baudrate of the hardware with the handle hHw
		m_LastLINErr = m_pPLinApi->GetHardwareParam(hHw, hwpBaudrate, &baudrate, 0);

		// Update hardware mode combobox
		if (mode == modMaster)
			cbbHwMode->ItemIndex = 1;
		else
			cbbHwMode->ItemIndex = 0;
		cbbHwModeChange(NULL);
		// Show the current baudrate in the combobox
		cbbBaudRates->Text = (baudrate != 0) ? IntToStr(baudrate) : IntToStr(m_wBaudrate);
	}
	else
	{
		btnInit->Enabled = false;
		btnIdent->Enabled = false;
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnHwRefreshClick(TObject *Sender)
{
	RefreshHardware();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::cbbHwModeChange(TObject *Sender)
{
	// Change the "Write" button text depending on the hardware mode
	if (cbbHwMode->ItemIndex == 1)
		btnWrite->Caption = SWriteAsMaster;
	else
		btnWrite->Caption = SWriteAsSlave;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnIdentClick(TObject *Sender)
{
	 // Get hardware device from the selected combobox item
	HLINHW hHw = (HLINHW) cbbChannel->Items->Objects[cbbChannel->ItemIndex];
	if (hHw != 0)
	{
		// Makes the corresponding LED on the adaptor blink
		m_pPLinApi->IdentifyHardware(hHw);
	}                                        
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnInitClick(TObject *Sender)
{
	// Do the connection
	if (DoLinConnect())
	{
		// Sets the connection status of the main-form
		SetConnectionStatus(true);
		// Update Global Frame Table LIN information
		m_pGFT->UpdateLinInfo(m_hClient, m_hHw, m_HwMode, &m_lMask);
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnReleaseClick(TObject *Sender)
{
	if (DoLinDisconnect()) 
	{
		// Stop reading timer
		tmrRead->Enabled = False;
		// Sets the connection status of the main form
		SetConnectionStatus(false);
		// Update Global Frame Table LIN information
		m_pGFT->UpdateLinInfo(m_hClient, m_hHw, m_HwMode, &m_lMask);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////
//	Global frames and filter message event-handlers

void __fastcall TMainForm::btnConfigureClick(TObject *Sender)
{
	// Open the "Global Frame Table" dialog box
	TGlobalFrameTableDialog* pDlg = new TGlobalFrameTableDialog(m_pGFT, this);
	__try
	{
		pDlg->ShowModal();
	}
	__finally
	{
		delete pDlg;
	}
	// Output filter information (as changes to Global Frame Table items modify it)
	btnFilterQueryClick(NULL);
	// Update the available frame IDs (i.e. the IDs combobox in the "Write Message" UI group)
	UpdateFrameIds();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::rdbFilterChanged(TObject *Sender)
{
	// Enable/disable filter fields
	if (rdbFilterCustom->Checked)
	{
		nudIdFrom->Enabled = true;
		nudIdTo->Enabled = true;
		txtFilterFrom->Enabled = true;
		txtFilterTo->Enabled = true;
	}
	else
	{
		nudIdFrom->Enabled = false;
		nudIdTo->Enabled = false;
		txtFilterFrom->Enabled = false;
		txtFilterTo->Enabled = false;
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::txtFilterFromExit(TObject *Sender)
{
	int newValue;

	// Compute new edited value
	if (txtFilterFrom->Text != "")
		newValue = StrToInt(txtFilterFrom->Text);
	else
		newValue = 0;

	if (newValue > 63)
		newValue = 63;
	else if (newValue < 0)
		newValue = 0;
	// Update Nud control
	nudIdFrom->Position = newValue;
	txtFilterFrom->Text = IntToStr(nudIdFrom->Position);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::txtFilterToExit(TObject *Sender)
{
	int newValue;

	// Compute new edited value
	if (txtFilterTo->Text != "")
		newValue = StrToInt(txtFilterTo->Text);
	else
		newValue = 0;              

	if (newValue > 63)
		newValue = 63;
	else if (newValue < 0)
		newValue = 0;
	// Update Nud control
	nudIdTo->Position = newValue;
	txtFilterTo->Text = IntToStr(nudIdTo->Position);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::txtFilterKeyPress(TObject *Sender, char &Key)
{
	// Convert the character to its upper-case equivalent
	Key = *(UpperCase((AnsiString)Key)).c_str();

	// The key is the Delete (Backspace) Key
	if (Key == 8)
		return;
	// The key is a number between 0-9
	if ((Key >= '0') && (Key <= '9'))
		return;

	// Key is not a number
	Key = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnFilterApplyClick(TObject *Sender)
{
	AnsiString strTemp;
	unsigned __int64 lMask;
	Byte nudFrom, nudTo;

	if (m_hHw != 0)
	{
		// Sets the according to the selected filter
		if (rdbFilterOpen->Checked == true)
		{
			// Set no filtering: all frames are received
			lMask = FRAME_FILTER_MASK;
			m_LastLINErr = m_pPLinApi->SetClientFilter(m_hClient, m_hHw, lMask);
			if (m_LastLINErr == errOK)
			{
				m_lMask = lMask;
				IncludeTextMessage("The filter was successfully opened.");
			}
		}
		else if (rdbFilterClose->Checked == true)
		{
			// Filters all: all frames are denied
			lMask = 0x0;
			m_LastLINErr = m_pPLinApi->SetClientFilter(m_hClient, m_hHw, lMask);
			if (m_LastLINErr == errOK)
			{
				m_lMask = lMask;
				IncludeTextMessage("The filter was successfully closed.");
			}
		}
		else
		{
			// Custom filtering (i.e. rdbFilterCustom.Checked): the frames for the given range will be opened
			nudFrom = (Byte) nudIdFrom->Position;
			nudTo = (Byte) nudIdTo->Position;
			m_LastLINErr = m_pPLinApi->RegisterFrameId(m_hClient, m_hHw, nudFrom, nudTo);
			if (m_LastLINErr == errOK)
			{
				strTemp = AnsiString::Format("The filter was customized. IDs from %d to %d will be received", ARRAYOFCONST((nudFrom, nudTo)));
				IncludeTextMessage(strTemp);
				m_LastLINErr = m_pPLinApi->GetClientFilter(m_hClient, m_hHw, &lMask);
				if (m_LastLINErr == errOK)
					m_lMask = lMask;
			}
		}
		// If successful, an information message is written; if an error occurs, an error message is displayed
		if (m_LastLINErr != errOK)
			MessageDlg(GetFormatedError(m_LastLINErr), mtError, TMsgDlgButtons() << mbOK, 0);
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnFilterQueryClick(TObject *Sender)
{
	// Retrieves the filter corresponding to the current Client-Hardware pair
	if (m_hHw != 0)
	{
		unsigned __int64 rcvMask;
		if (m_pPLinApi->GetClientFilter(m_hClient, m_hHw, &rcvMask) == errOK)
		{
			double power = 0;
			AnsiString strTmp;
			Extended temp = rcvMask;
			// convert to binary string
			for (int bit = 63; bit > -1; bit--)
			{
				power = pow(2, bit);
				if (temp >= power )
				{
					strTmp = strTmp + "1";
					temp = temp - power;
				}
				else
					strTmp = strTmp + "0";
			}
			// Add message
			IncludeTextMessage(AnsiString::Format("The Status of the filter is %.064s.", ARRAYOFCONST((strTmp))));
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////
//	Read message event-handlers

void __fastcall TMainForm::rdbTimerChanged(TObject *Sender)
{
	// According to the kind of reading, a timer or a button will be enabled
	if (btnRelease->Enabled && rdbTimer->Checked)
		tmrRead->Enabled = true;
	else
		tmrRead->Enabled = false;

	btnRead->Enabled = (!rdbTimer->Checked && btnRelease->Enabled);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::chbShowPeriodClick(TObject *Sender)
{
	for (int i = 0; i < m_LastMsgsList->Count; i++)
		((TMessageStatus*) m_LastMsgsList->Items[i])->ShowPeriod = chbShowPeriod->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnReadClick(TObject *Sender)
{
	ReadMessages();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnMsgClearClick(TObject *Sender)
{
    // Clear information shown in the messages ListView
	m_LastMsgsList->Clear();
	lstMessages->Clear();
}


//////////////////////////////////////////////////////////////////////////////////////////////
//	Timer event-handlers

void __fastcall TMainForm::tmrReadTimer(TObject *Sender)
{
	// Checks if in the receive-queue are currently messages for read
	ReadMessages();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::tmrDisplayTimer(TObject *Sender)
{
	// Update display of received messages
	DisplayMessages();
}


//////////////////////////////////////////////////////////////////////////////////////////////
//	Write message event-handlers

void __fastcall TMainForm::cbbIDChange(TObject *Sender)
{
	TFrameDefinition *pFrame = (TFrameDefinition*) cbbID->Items->Objects[cbbID->ItemIndex];
	// Check if a global frame is selected
	if (pFrame != NULL)
	{
		// Update components according to the selected frame informations
		txtID->Text = pFrame->ProtectedId;
		txtLength->Text = pFrame->LengthAsString;
		nudLength->Position = pFrame->Length;

		switch(pFrame->Direction)
		{
			case dirDisabled:
				cbbDirection->ItemIndex = 0;
				break;
			case dirPublisher:
				cbbDirection->ItemIndex = 1;
				break;
			case dirSubscriber:
				cbbDirection->ItemIndex = 2;
				break;
			case dirSubscriberAutoLength:
				cbbDirection->ItemIndex = 3;
				break;
		}

		switch (pFrame->ChecksumType)
		{
			case cstAuto:
				cbbCST->ItemIndex = 0;
				break;
			case cstClassic:
				cbbCST->ItemIndex = 1;
				break;
			case cstEnhanced:
				cbbCST->ItemIndex = 2;
				break;
			case cstCustom:
				cbbCST->ItemIndex = 3;
				break;
		}
		// Force update on Data fields (both Length and Direction impact on Enabled state of these components)
		// if length value is not changed but direction switch from subscriber to publisher
		// data textfield would not be disabled
		nudLengthClick(NULL, btNext);
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::txtData0KeyPress(TObject *Sender, char &Key)
{
	// We convert the character to its upper-case equivalent
	Key = *(UpperCase((AnsiString)Key)).c_str();

	// The key is the Delete (Backspace) Key
	if (Key == 8)
		return;

	// The key is a number between 0-9
	if ((Key > 47) && (Key < 58))
		return;

	// The key is a character between A-F
	if ((Key > 64) && (Key < 71))
		return;

	// Key is neither a number nor a character between A(a) and F(f)
	Key = NULL;
}    
//---------------------------------------------------------------------------
void __fastcall TMainForm::txtData0Exit(TObject *Sender)
{
	TEdit *CurrentEdit;

	// Checks the length of the given value
	if(String(Sender->ClassName()) == "TEdit"){
		CurrentEdit = (TEdit*)Sender;
		while(CurrentEdit->Text.Length() != 2)
            CurrentEdit->Text = ("0" + CurrentEdit->Text);
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnWriteClick(TObject *Sender)
{
	// Assert that a frame ID has been selected
	if (cbbID->ItemIndex == -1)
	{
		cbbID->SetFocus();
		return;
	}

	 // Get the TFrameDefinition object associated to the selected frame ID
	TFrameDefinition *pFrame = (TFrameDefinition*) cbbID->Items->Objects[cbbID->ItemIndex];

	// Create a new LIN frame message and copy the data
	TLINMsg msg;
	msg.FrameId = (Byte) pFrame->ProtectedId;
	msg.Direction = (TLINDirection) cbbDirection->Items->Objects[cbbDirection->ItemIndex];
	msg.ChecksumType = (TLINChecksumType) cbbCST->Items->Objects[cbbCST->ItemIndex];
	msg.Length = (Byte) nudLength->Position;
	// Fill data array
	msg.Data[0] = (Byte)StrToInt("0x" + txtData0->Text);
	msg.Data[1] = (Byte)StrToInt("0x" + txtData1->Text);
	msg.Data[2] = (Byte)StrToInt("0x" + txtData2->Text);
	msg.Data[3] = (Byte)StrToInt("0x" + txtData3->Text);
	msg.Data[4] = (Byte)StrToInt("0x" + txtData4->Text);
	msg.Data[5] = (Byte)StrToInt("0x" + txtData5->Text);
	msg.Data[6] = (Byte)StrToInt("0x" + txtData6->Text);
	msg.Data[7] = (Byte)StrToInt("0x" + txtData7->Text);

	// Check if the hardware is initialized as Master
	if (m_HwMode == modMaster)
	{
		// Calculate the checksum for the message
		m_pPLinApi->CalculateChecksum(&msg);
		// Try to send the LIN frame with LIN_Write()
		m_LastLINErr = m_pPLinApi->Write(m_hClient, m_hHw, &msg);
	}
	else
	{
		// If the hardare is initialize as Slave, only update the data in the LIN frame
		m_LastLINErr = m_pPLinApi->UpdateByteArray(m_hClient, m_hHw, pFrame->Id, 0, msg.Length, msg.Data);
	}
	// Show error if any
	if (m_LastLINErr != errOK)
		MessageDlg(GetFormatedError(m_LastLINErr), mtError, TMsgDlgButtons() << mbOK, 0);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::nudLengthClick(TObject *Sender, TUDBtnType Button)
{
	txtData0->Enabled = false;
	txtData1->Enabled = false;
	txtData2->Enabled = false;
	txtData3->Enabled = false;
	txtData4->Enabled = false;
	txtData5->Enabled = false;
	txtData6->Enabled = false;
	txtData7->Enabled = false;
	// Only Publisher direction allows editing of Data textboxes
	if (cbbDirection->ItemIndex != 1)
    	return;
	// Enable the same number of data textbox as the nudLength value
	switch (nudLength->Position)
	{
		case 1:
			txtData0->Enabled = true;
			break;
		case 2:
			txtData0->Enabled = true;
			txtData1->Enabled = true; 
			break;
		case 3:
			txtData0->Enabled = true;
			txtData1->Enabled = true;
			txtData2->Enabled = true; 
			break;
		case 4:
			txtData0->Enabled = true;
			txtData1->Enabled = true;
			txtData2->Enabled = true;
			txtData3->Enabled = true; 
			break;
		case 5:
            txtData0->Enabled = true;
			txtData1->Enabled = true;
            txtData2->Enabled = true;
			txtData3->Enabled = true;
            txtData4->Enabled = true;  
			break;
		case 6:
			txtData0->Enabled = true;
			txtData1->Enabled = true;
			txtData2->Enabled = true;
			txtData3->Enabled = true;
			txtData4->Enabled = true;
			txtData5->Enabled = true;  
			break;
		case 7:
			txtData0->Enabled = true;
			txtData1->Enabled = true;
			txtData2->Enabled = true;
			txtData3->Enabled = true;
			txtData4->Enabled = true;
            txtData5->Enabled = true;
			txtData6->Enabled = true;  
			break;
		case 8:
            txtData0->Enabled = true;
            txtData1->Enabled = true;
            txtData2->Enabled = true;
			txtData3->Enabled = true;
			txtData4->Enabled = true;
			txtData5->Enabled = true;
			txtData6->Enabled = true;
			txtData7->Enabled = true;
			break;
	}
}


///////////////////////////////////////////////////////////////////////////
// Information group event-handlers

void __fastcall TMainForm::btnGetVersionsClick(TObject *Sender)
{
	TLINVersion lpVersion;
	char buffer[255];
	AnsiString info, strTemp;
	int iPos;

	// We get the vesion of the PLIN-API
	m_LastLINErr = m_pPLinApi->GetVersion(&lpVersion);
	if (m_LastLINErr == errOK)
	{
		strTemp = AnsiString::Format("API Version: %d.%d.%d.%d", ARRAYOFCONST((lpVersion.Major, lpVersion.Minor,
			lpVersion.Build, lpVersion.Revision)));
		IncludeTextMessage(strTemp);
		// We get the driver version
		m_LastLINErr = m_pPLinApi->GetVersionInfo(buffer, 255);
		if (m_LastLINErr == errOK)
		{
			IncludeTextMessage("Channel/Driver Version: ");

			// Because this information contains line control characters (several lines)
            // we split this also in several entries in the Information listbox
			info = (AnsiString)buffer;
			while (info != "")
			{
				iPos = info.Pos("\n");
				if (iPos == 0)
					iPos = info.Length();
				IncludeTextMessage("     * " + info.SubString(1, iPos));
				info.Delete(1, iPos);
			}
		}
	}
	// If an error occurred, a message is shown
	if (m_LastLINErr != errOK)
		MessageDlg(GetFormatedError(m_LastLINErr), mtError, TMsgDlgButtons() << mbOK, 0);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnInfoClearClick(TObject *Sender)
{
	// The information contained in the Information listbox is cleared
	lbxInfo->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnStatusClick(TObject *Sender)
{
	TLINHardwareStatus lStatus;
	// Retrieves the status of the LIN Bus and outputs its state in the information listView
	m_LastLINErr = m_pPLinApi->GetStatus(m_hHw, &lStatus);
	if (m_LastLINErr == errOK)
	{
		switch(lStatus.Status)
		{
			case hwsNotInitialized:
				IncludeTextMessage("Hardware: Not Initialized");
				break;
			case hwsAutobaudrate:
				IncludeTextMessage("Hardware: Baudrate Detection");
				break;
			case hwsActive:
				IncludeTextMessage("Bus: Active");
				break;
			case hwsSleep:
				IncludeTextMessage("Bus: Sleep");
				break;
			case hwsShortGround:
				IncludeTextMessage("Bus-Line: Shorted Ground");
				break;
			case hwsVBatMissing:
				IncludeTextMessage("Hardware: Vbat Missing");
				break;
		}
	}
	else
		MessageDlg(GetFormatedError(m_LastLINErr), mtError, TMsgDlgButtons() << mbOK, 0);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnResetClick(TObject *Sender)
{
    // Flushes the Receive Queue of the Client and resets its counters. 
	m_LastLINErr = m_pPLinApi->ResetClient(m_hClient);

	// If it fails, a error message is shown
	if (m_LastLINErr != errOK)
		MessageDlg(GetFormatedError(m_LastLINErr), mtError, TMsgDlgButtons() << mbOK, 0);
	else
	{
		// clears the message list-view
		btnMsgClear->Click();
		IncludeTextMessage("Receive queue and counters successfully reset");
	}
}


///////////////////////////////////////////////////////////////////////////
// LIN functions

void TMainForm::RefreshHardware()
{
	Byte i;
	HLINHW *hwHandles;
	WORD lwBuffSize;
	int count, hwType, devNo, channel, mode;
	TLINError err;
	HLINHW hHw;
	AnsiString str;

	// Get the required buffer length
	count = 0;
	m_pPLinApi->GetAvailableHardware(NULL, 0, &count);
	if (count == 0)   // Use default value if either no hw is connected or an unexpected error occurred
		count = 16;
	hwHandles = new HLINHW[count];
	lwBuffSize = (WORD)(count * sizeof(HLINHW));

	// Get all available LIN hardware.
	err = m_pPLinApi->GetAvailableHardware(hwHandles, lwBuffSize, &count);
	if (err == errOK)
	{
		// combobox ItemData only holds DWORD, no need to free resources
		cbbChannel->Clear();
		// If no error occurs
		if (count == 0)
		{
			// No LIN hardware was found.
			// Show an empty entry
			hHw = 0;
			cbbChannel->AddItem("<No hardware found>", NULL);
		}
		// For each found LIN hardware
		for (i = 0; i < count; i++)
		{
			// Get the handle of the hardware.
			hHw = hwHandles[i];
			// Read the type of the hardware with the handle hHw.
			m_pPLinApi->GetHardwareParam(hHw, hwpType, &hwType, 0);
			// Read the device number of the hardware with the handle hHw.
			m_pPLinApi->GetHardwareParam(hHw, hwpDeviceNumber, &devNo, 0);
			// Read the channel number of the hardware with the handle hHw.
			m_pPLinApi->GetHardwareParam(hHw, hwpChannelNumber, &channel, 0);
			// Read the mode of the hardware with the handle hHw (Master, Slave or None).
			m_pPLinApi->GetHardwareParam(hHw, hwpMode, &mode, 0);

			// Create a comboboxItem
			// If the hardware type is a knowing hardware
			// show the name of that in the label of the entry.
			switch(hwType)
			{
				case LIN_HW_TYPE_USB_PRO:
					str = Format("PCAN-USB Pro - dev. %d, chan. %d", ARRAYOFCONST((devNo, channel)));
					break;
				case LIN_HW_TYPE_USB_PRO_FD:
					// Show as unknown hardware
					str = Format("PCAN-USB Pro FD - dev. %d, chan. %d", ARRAYOFCONST((devNo, channel)));
					break;
				case LIN_HW_TYPE_PLIN_USB:
					// Show as unknown hardware
					str = Format("PLIN-USB - dev. %d, chan. %d", ARRAYOFCONST((devNo, channel)));
					break;
				default:
					// Show as unknown hardware
					str = Format("Unknown - dev. %d, chan. %d", ARRAYOFCONST((devNo, channel)));
					break;
			}

			cbbChannel->AddItem(str, (TObject *)hHw);
		}
		cbbChannel->ItemIndex = 0;   
		cbbChannelChange(cbbChannel);   // call change selection
	}
	else
		MessageDlg(GetFormatedError(m_LastLINErr), mtError, TMsgDlgButtons() << mbOK, 0);
	delete hwHandles;
}

bool TMainForm::DoLinConnect()
{
	bool res = false;

	if (m_hHw != 0)
	{
		// If a connection to hardware already exits
		// disconnect this connection first.
		if (DoLinDisconnect() == false)
			return res;
	}
	// Get the selected Hardware handle from the combobox item
	HLINHW hHw = (HLINHW) cbbChannel->Items->Objects[cbbChannel->ItemIndex];
	if (hHw != 0)
	{
		if (m_hClient == 0)
			// Register this application as a LIN client
			m_LastLINErr = m_pPLinApi->RegisterClient("PLIN-EXAMPLE", (DWORD)Handle, &m_hClient);
		// The local hardware handle is valid, get the current mode of the hardware
		int mode;
		m_pPLinApi->GetHardwareParam(hHw, hwpMode, &mode, 0);
		// Get the current baudrate of the hardware
		int currentBaudrate;
		m_pPLinApi->GetHardwareParam(hHw, hwpBaudrate, &currentBaudrate, 0);
		// Try to connect the application client to the hardware with the local handle
		m_LastLINErr = m_pPLinApi->ConnectClient(m_hClient, hHw);
		if (m_LastLINErr == errOK)
		{
			// If the connection successful, assign the local handle to the member variable
			m_hHw = hHw;
			// Get the selected hardware mode
			if (cbbHwMode->ItemIndex == 1)
				mode = modMaster;
			else
				mode = modSlave;
			// Get the selected baudrate
			WORD baudrate = (WORD) StrToIntDef(cbbBaudRates->Text, 0);
			// Get the selected hardware channel
			if ((mode == modNone) || ((WORD)currentBaudrate != baudrate))
			{
				// If the current hardware is not initialized try to intialize the hardware with mode and baudrate
				m_LastLINErr = m_pPLinApi->InitializeHardware(m_hClient, m_hHw, mode, baudrate);
			}
			if (m_LastLINErr == errOK)
			{
				// Assign the Hardware Mode to member variable
				m_HwMode = mode;
				// Assign the baudrate index to member variable
				m_wBaudrate = baudrate;
				// Set the client filter with the mask
				m_LastLINErr = m_pPLinApi->SetClientFilter(m_hClient, m_hHw, m_lMask);
				// Read the frame table from the connected hardware
				ReadFrameTableFromHw();
				// Reset the last LIN error code to default
				m_LastLINErr = errOK;
				res = true;
			}
			else
			{
				// An error occurred while initializing hardware,
				// set the member variable to default
				m_hHw = 0;
			}
		}
		else
		{
			// The local hardware handle is invalid and/or an error occurs while connecting
			// hardware with client, Set the member variable to default
			m_hHw = 0;
		}

		// Check if any LIN error occurred
		if (m_LastLINErr != errOK)
			MessageDlg(GetFormatedError(m_LastLINErr), mtError, TMsgDlgButtons() << mbOK, 0);

	}
	else // Should never occur
		m_hHw = 0; // But if it occurs, set handle to default

	return res;
}

bool TMainForm::DoLinDisconnect()
{
	// If the application was registered with LIN as client.
	if (m_hHw != 0)
	{
		// The client is already connected to a LIN hardware.
		// Before disconnecting from the hardware, check
		// the connected clients and determine whether the
		// hardware configuration has to be reset or not

		bool otherClient = false;
		bool ownClient = false;

		// Get the connected clients from the LIN hardware
		BYTE hClients[255];
		m_LastLINErr = m_pPLinApi->GetHardwareParam(m_hHw, hwpConnectedClients, hClients, sizeof(hClients));
		if (m_LastLINErr == errOK)
		{
			// No errors, check all client handles
			for (int i = 0; i < 255; i++)
			{
				// If client handle is invalid
				if (hClients[i] == 0)
					continue;
				// Set the boolean to true if the handle isn't the handle of this application.
				// When the boolean is already true it can never bes set to false.
				otherClient = otherClient || (hClients[i] != m_hClient);
				// Set the boolean to true if the handle is the handle of this application.
				// When the boolean is already true it can never bes set to false.
				ownClient = ownClient || (hClients[i] == m_hClient);
			}
		}
		// If another application is also connected to the LIN hardware do not reset the configuration
		if (!otherClient)
		{
			// No other application connected, reset the configuration of the LIN hardware
			m_pPLinApi->ResetHardwareConfig(m_hClient, m_hHw);
		}
		// If this application is connected to the hardware then disconnect the client
		if (ownClient)
		{
			m_LastLINErr = m_pPLinApi->DisconnectClient(m_hClient, m_hHw);
			if (m_LastLINErr == errOK)
			{
				m_hHw = 0;
			}
			else
			{
				// Error while disconnecting from hardware
				MessageDlg(GetFormatedError(m_LastLINErr), mtError, TMsgDlgButtons() << mbOK, 0);
				return false;
			}
		}
	}
	return true;
}

void TMainForm::ReadFrameTableFromHw()
{
	TLINFrameEntry frameEntry;
	TLINError err;
	unsigned __int64 mask;
	TFrameDefinition* pFrame;

	// Initialize the member variable for the client mask with 0
	m_lMask = 0;
	// Iterate the Global Frame Table
	for (int i = 0; i < m_pGFT->Count; i++)
	{
		pFrame = m_pGFT->Frames[i];
		// Before a frame entry can be read from the hardware, the frame ID
		// of the wanted entry must be set
		frameEntry.FrameId = pFrame->Id;
		// Read the information of the specified frame entry from the hardware.
		err = m_pPLinApi->GetFrameEntry(m_hHw, &frameEntry);
		// Check the result value of the LinApi function call.
		if (err == errOK)
		{
			// LIN-API function call successful, copy the frame entry information
			// to the TFrameDefinition object
			pFrame->SetLength(frameEntry.Length);
			pFrame->SetDirection(frameEntry.Direction);
			pFrame->SetChecksumType(frameEntry.ChecksumType);
			if (pFrame->Direction != dirDisabled)
			{
				// If the direction is not disabled then set the in the client
				// filter mask
				mask = ((unsigned __int64)1 << i) & FRAME_FILTER_MASK;
				m_lMask = m_lMask | mask;
			}
		}
	}
	// If the Client and Hardware handles are valid
	if ((m_hClient != 0) && (m_hHw != 0))
		// Set the client filter
		m_pPLinApi->SetClientFilter(m_hClient, m_hHw, m_lMask);
	// Updates the displayed frame IDs
	UpdateFrameIds();
}
	
///////////////////////////////////////////////////////////////////////////
// LIN Message processing functions

void TMainForm::ReadMessages()
{
	TLINRcvMsg msg;

	// We read at least one time the queue looking for messages.
	// If a message is found, we look again trying to find more.
	// If the queue is empty or an error occurs, we exit the
	// do-while loop
	do
	{
		m_LastLINErr = m_pPLinApi->Read(m_hClient, &msg);
		// If at least one frame has been received by the LIN-API,
		// check if the received frame has the standard type, in which
		// case it is ignored
		if (msg.Type != mstStandard)
			continue;

		if (m_LastLINErr == errOK)
			ProcessMessage(msg);

	} while (btnRelease->Enabled && (!(m_LastLINErr & errRcvQueueEmpty)));
}

void TMainForm::ProcessMessage(TLINRcvMsg linMsg)
{
	int pos;
	TMessageStatus *pMsg;

	// We search if a message (Same ID and Type) has
	// already been received or if this is a new message
	//
	for( int i = 0 ; i < m_LastMsgsList->Count ; i++ )
	{
		pMsg = (TMessageStatus*) m_LastMsgsList->Items[i];
		if (pMsg->LINMsg.FrameId == linMsg.FrameId)
		{
			// Modify the message and exit
			//
			pMsg->Update(linMsg);
			return;
		}
	}
	// Message not found. It will be created
	//
	InsertMsgEntry(linMsg);
}

void TMainForm::InsertMsgEntry(TLINRcvMsg newMsg)
{
	// We add this status in the last message list
	TMessageStatus* pMsg = new TMessageStatus(newMsg, lstMessages->Items->Count);
	m_LastMsgsList->Add(pMsg);

	// Search and retrieve the ID in the global frame table associated with the frame Protected-ID
	AnsiString strId;
	for (int i = 0; i < m_pGFT->Count; i++)
	{
		if (pMsg->LINMsg.FrameId == (BYTE) m_pGFT->Frames[i]->ProtectedId )
		{
			strId = m_pGFT->Frames[i]->Id;
			break;
		}
	}

	// Add the new ListView item with the ID of the message
	TListItem* pItem = lstMessages->Items->Add();
	pItem->Caption = strId;
	// We set the length of the message
	pItem->SubItems->Add(IntToStr(pMsg->LINMsg.Length));
	// We set the data of the message.
	pItem->SubItems->Add(pMsg->DataString);
	// We set the message count message (this is the first, so count is 1)
	pItem->SubItems->Add(IntToStr(pMsg->Count));
	// Add time stamp information if needed
	pItem->SubItems->Add(pMsg->TimeString);
	// We set the direction of the message
	pItem->SubItems->Add(pMsg->DirectionString);
	// We set the error of the message
	pItem->SubItems->Add(pMsg->ErrorString);
	// We set the CST of the message
	pItem->SubItems->Add(pMsg->CSTString);
	// We set the CRC of the message
	pItem->SubItems->Add(pMsg->ChecksumString);
}

void TMainForm::DisplayMessages()
{
	TMessageStatus *pMsg;
	TListItem *pItem;
	for (int i = 0 ; i < m_LastMsgsList->Count ; i++)
	{
		pMsg = (TMessageStatus*) m_LastMsgsList->Items[i];
		if (pMsg->WasChanged)
		{
			pItem = lstMessages->Items->Item[i];
			// Update the length of the message
			pItem->SubItems->Strings[0] = IntToStr(pMsg->LINMsg.Length);
			// Update the data of the message.
			pItem->SubItems->Strings[1] = pMsg->DataString;
			// Update the message count message
			pItem->SubItems->Strings[2] = IntToStr(pMsg->Count);
			// Update time stamp information
			pItem->SubItems->Strings[3] = pMsg->TimeString;
			// Update the direction of the message
			pItem->SubItems->Strings[4] = pMsg->DirectionString;
			// Update the error of the message
			pItem->SubItems->Strings[5] = pMsg->ErrorString;
			// Update the CST of the message
			pItem->SubItems->Strings[6] = pMsg->CSTString;
			// Update the CRC of the message
			pItem->SubItems->Strings[7] = pMsg->ChecksumString;

			pMsg->WasChanged = false;
		}
	}
}


///////////////////////////////////////////////////////////////////////////
// Helper functions

void TMainForm::UpdateFrameIds()
{
	TFrameDefinition *pFrame, *pSelectedFrame;

	pFrame = pSelectedFrame = NULL;
	// Gets the selected ID if it exists before clearing combobox
	if (cbbID->ItemIndex != -1)
		pSelectedFrame = (TFrameDefinition*) cbbID->Items->Objects[cbbID->ItemIndex];

	// Clears and populates FrameID combobox with global frame IDs
	cbbID->Clear();
	for (int i = 0; i < m_pGFT->Count ; i++)
	{
		pFrame = m_pGFT->Frames[i];
		// Add only frames that are not disabled
		if (pFrame->Direction == dirDisabled)
			continue;
		// Add frame and data
		cbbID->Items->AddObject(pFrame->Id, (TObject*) pFrame);
		// Check if the new item was selected before the update
		if (pSelectedFrame == pFrame)
			cbbID->ItemIndex = cbbID->Items->Count - 1;
	}
}

void TMainForm::IncludeTextMessage(AnsiString strMsg)
{
	lbxInfo->Items->Add(strMsg);
	lbxInfo->ItemIndex = lbxInfo->Items->Count - 1;
}

void TMainForm::SetConnectionStatus(bool bConnected)
{
	// Buttons
	btnInit->Enabled = !bConnected;
	btnConfigure->Enabled = bConnected;
	btnRead->Enabled = bConnected && rdbManual->Checked;
	btnWrite->Enabled = bConnected;
	btnRelease->Enabled = bConnected;
	btnFilterApply->Enabled = bConnected;
	btnFilterQuery->Enabled = bConnected;
	btnGetVersions->Enabled = bConnected;
	btnHwRefresh->Enabled = !bConnected;
    btnStatus->Enabled = bConnected;
    btnReset->Enabled = bConnected;

    // ComboBoxes
	cbbBaudRates->Enabled = !bConnected;
	cbbChannel->Enabled = !bConnected;
	cbbHwMode->Enabled = !bConnected;

    // Hardware configuration and read mode
	if (!bConnected)
		cbbChannelChange(NULL);
	else
		rdbTimerChanged(NULL);

	// Display messages in grid
	tmrDisplay->Enabled = bConnected;
}

AnsiString TMainForm::GetFormatedError(TLINError error)
{
	AnsiString result = EmptyStr;
	char buffer[256];

	memset(buffer,'\0',256);
	// If any error occurred, display the error text in a message box
	// 0x00 = Neutral
	// 0x07 = Language German
	// 0x09 = Language English
	if (m_pPLinApi->GetErrorText(error, 0x09, buffer, 255) != errOK)
		result = Format("An error occurred. Errorcode's text (%d) couldn't be retrieved", ARRAYOFCONST((Integer(error))));
	else
		result = buffer;

	return result;
}

