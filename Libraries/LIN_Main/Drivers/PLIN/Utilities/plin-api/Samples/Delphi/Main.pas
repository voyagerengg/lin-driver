unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, PLinApi, GlobalFrameTable, Math;

const
  SHardwareNothingFound   = '<No hardware found>';
  SHardwareTypeLIN        = 'PCAN-USB Pro';
  SHardwareTypeLINFD      = 'PCAN-USB Pro FD';
  SHardwareTypePLIN       = 'PLIN-USB';
  SHardwareTypeUnkown     = 'Unknown';
  SPLinClientName         = 'PLIN_EXAMPLE';
  SWriteAsMaster          = 'Transmit';
  SWriteAsSlave           = 'Publish';

type
  // TMessageStatus class used to show LIN Messages in a ListView control
  TMessageStatus = class(TObject)
  private
    // The received LIN message
    m_msg: TLINRcvMsg;
    // Timestamp of a previously received message
    m_oldTimeStamp: UInt64;
    // index of the message in the ListView component
    m_iIndex: Integer;
    // Number of LIN message received with the same frame ID
    m_nCount: Integer;
    // Defines if the timestamp is displayed as a time period
    m_bShowPeriod: Boolean;
    // Defines if the message has been modified and its display needs to be updated
    m_bWasChanged: Boolean;

    // Property getters/setters
    function GetDirectionString: String;
    function GetCSTString: String;
    function GetChecksumString: String;
    function GetErrorString: String;
    function GetProtectedIdString: String;
    function GetDataString: String;
    procedure SetShowPeriod(value: Boolean);
    function GetTimeString: String;
  public
    /// <summary>
    /// Creates a new TMessageStatus object
    /// </summary>
    /// <param name="linMsg">received LIN message</param>
    /// <param name="listIndex">index of the message in the ListView</param>
    constructor Create(const linMsg: TLINRcvMsg; const listIndex: Integer);
    /// <summary>
    /// Updates an existing TMessageStatus with a new LIN message
    /// </summary>
    /// <param name="linMsg">LIN message to update</param>
    procedure Update(linMsg: TLINRcvMsg);

    // The received LIN message
    property LINMsg: TLINRcvMsg read m_msg;
    // Index of the message in the ListView
    property Position: Integer read m_iIndex;
    // Direction of the LIN message as a string
    property DirectionString: String read GetDirectionString;
    // Checksum type of the LIN message as a string
    property CSTString: String read GetCSTString;
    // Checksum of the LIN message as a string
    property ChecksumString: String read GetChecksumString;
    // Error field of the LIN message as a string
    property ErrorString: String read GetErrorString;
    // Protected ID of the LIN message as a string
    property PIdString: String read GetProtectedIdString;
    // Data fields of the LIN message as a string
    property DataString: String read GetDataString;
    // Number of LIN messages received with the same frame ID
    property Count: Integer read m_nCount;
    // States wether the timestamp is displayed as a period or not
    property ShowPeriod: Boolean read m_bShowPeriod write SetShowPeriod;
    // Defines if the LIN message has been modified
    property WasChanged: Boolean read m_bWasChanged write m_bWasChanged;
    // The timestamp or period of the LIN message
    property TimeString: String read GetTimeString;
  end;

  TMainForm = class(TForm)
    tmrDisplay: TTimer;
    tmrRead: TTimer;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cbbChannel: TComboBox;
    btnHwRefresh: TButton;
    Label2: TLabel;
    cbbBaudRates: TComboBox;
    Label3: TLabel;
    cbbHwMode: TComboBox;
    btnIdent: TButton;
    btnInit: TButton;
    btnRelease: TButton;
    GroupBox2: TGroupBox;
    btnConfigure: TButton;
    rdbFilterOpen: TRadioButton;
    rdbFilterClose: TRadioButton;
    rdbFilterCustom: TRadioButton;
    Label4: TLabel;
    txtFilterFrom: TEdit;
    Label5: TLabel;
    nudIdFrom: TUpDown;
    txtFilterTo: TEdit;
    nudIdTo: TUpDown;
    btnFilterApply: TButton;
    btnFilterQuery: TButton;
    GroupBox3: TGroupBox;
    rdbTimer: TRadioButton;
    rdbManual: TRadioButton;
    chbShowPeriod: TCheckBox;
    lstMessages: TListView;
    btnRead: TButton;
    btnMsgClear: TButton;
    GroupBox4: TGroupBox;
    Label6: TLabel;
    cbbID: TComboBox;
    Label7: TLabel;
    txtID: TEdit;
    Label8: TLabel;
    cbbDirection: TComboBox;
    Label9: TLabel;
    cbbCST: TComboBox;
    txtLength: TEdit;
    Label10: TLabel;
    nudLength: TUpDown;
    Label11: TLabel;
    txtData0: TEdit;
    txtData1: TEdit;
    txtData2: TEdit;
    txtData3: TEdit;
    txtData4: TEdit;
    txtData5: TEdit;
    txtData6: TEdit;
    txtData7: TEdit;
    btnWrite: TButton;
    GroupBox5: TGroupBox;
    btnGetVersions: TButton;
    btnInfoClear: TButton;
    btnStatus: TButton;
    btnReset: TButton;
    lbxInfo: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbbChannelChange(Sender: TObject);
    procedure btnHwRefreshClick(Sender: TObject);
    procedure cbbHwModeChange(Sender: TObject);
    procedure btnIdentClick(Sender: TObject);
    procedure btnInitClick(Sender: TObject);
    procedure rdbFilterChanged(Sender: TObject);
    procedure txtFilterFromExit(Sender: TObject);
    procedure txtFilterToExit(Sender: TObject);
    procedure btnFilterApplyClick(Sender: TObject);
    procedure btnFilterQueryClick(Sender: TObject);
    procedure chbShowPeriodClick(Sender: TObject);
    procedure btnReadClick(Sender: TObject);
    procedure btnMsgClearClick(Sender: TObject);
    procedure cbbIDChange(Sender: TObject);
    procedure txtFilterKeyPress(Sender: TObject; var Key: Char);
    procedure tmrReadTimer(Sender: TObject);
    procedure tmrDisplayTimer(Sender: TObject);
    procedure txtData0Exit(Sender: TObject);
    procedure txtData0KeyPress(Sender: TObject; var Key: Char);
    procedure btnWriteClick(Sender: TObject);
    procedure btnGetVersionsClick(Sender: TObject);
    procedure btnInfoClearClick(Sender: TObject);
    procedure btnStatusClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnReleaseClick(Sender: TObject);
    procedure btnConfigureClick(Sender: TObject);
    procedure rdbTimerChanged(Sender: TObject);
    procedure nudLengthClick(Sender: TObject; Button: TUDBtnType);
  private
    // Constant value that specifies the mask of the client filter (64bit)
    const FRAME_FILTER_MASK = UInt64($FFFFFFFFFFFFFFFF);

  private
    // Client handle
    m_hClient: HLINCLIENT;
    // Hardware handle
    m_hHw: HLINHW;
    // LIN Hardware Modus (Master/Slave)
    m_HwMode: TLINHardwareMode;
    // Client filter mask
    m_lMask: UInt64;
    // Baudrate Index of Hardware
    m_wBaudrate: Word;
    // Last LIN error
    m_LastLINErr: TLINError;
    // Global Frame Table object to hold all frames definitions.
    m_gft: TGlobalFrameTable;
    // Stores the status of received messages for its display
    m_LastMsgsList: TList;

    //////////////////////////////////////////////////////////////////////////
    // LIN functions

    // Updates the combobox 'cbbChannel' with currently available hardwares
    procedure RefreshHardware;
    /// Connects to the hardware with the setting values
    /// from the connection groupbox.
    function DoLinConnect: Boolean;
    /// Disconnects an existing connection to a LIN hardware and returns
    /// True if disconnection finished succesfully or if no connection exists.
    /// Returns False if the current connection can not be disconnected.
    function DoLinDisconnect: Boolean;

    /// Reads all values from the frame table of the hardware
    /// and assign it to the GlobalFrameTable. Also refresh
    /// the Global Frame Table ListView with that values.
    procedure ReadFrameTableFromHw;

    ///////////////////////////////////////////////////////////////////////////
    // LIN message processing functions

    // Function for reading PLIN messages
    procedure ReadMessages;
    // Processes a received message, in order to show it in the Message-ListView
    procedure ProcessMessage(const linMsg: TLINRcvMsg);
    // Inserts a new entry for a new message in the Message-ListView
    procedure InsertMsgEntry(const newMsg: TLINRcvMsg);
    // Displays and updates LIN messages in the Message-ListView
    procedure DisplayMessages;

    ///////////////////////////////////////////////////////////////////////////
    // Helper functions

    // Initializes UI controls
    procedure InitializeControls;
    //  Updates Frame IDs with values from the global frame table
    procedure UpdateFrameIds;
    // Includes a new line of text into the information Listview
    procedure IncludeTextMessage(strMsg: String);
    // Activates/deactivates the different controls of the main-form according
    // to the current connection status
    procedure SetConnectionStatus(bConnected: Boolean);
    // Returns a LIN error as a formatted string
    function GetFormatedError(error: TLINError): String;
    // Shows a messageBox with a LIN error as a formatted string
    procedure MessageBoxErrorShow(error: TLINError);

  end;

var
  MainForm: TMainForm;

implementation

uses
  FrameDlg;

{ TMessageStatus }

constructor TMessageStatus.Create(const linMsg: TLINRcvMsg; const listIndex: Integer);
begin
  m_msg := linMsg;
  m_oldTimeStamp := linMsg.TimeStamp;
  m_iIndex := listIndex;
  m_nCount := 1;
  m_bShowPeriod := True;
  m_bWasChanged := False;
end;

procedure TMessageStatus.Update(linMsg: TLINRcvMsg);
begin
  m_oldTimeStamp := m_msg.TimeStamp;
  m_msg := linMsg;
  Inc(m_nCount);
  m_bWasChanged := True;
end;

function TMessageStatus.GetDirectionString: String;
begin
  Result := DirectionToString(m_msg.Direction);
end;

function TMessageStatus.GetCSTString: String;
begin
  Result := ChecksumTypeToString(m_msg.ChecksumType);
end;

function TMessageStatus.GetChecksumString: String;
begin
  Result  :=  Format('%Xh', [m_msg.Checksum]);
end;

function TMessageStatus.GetErrorString: String;
begin
  if (Integer(m_msg.ErrorFlags) = 0) then
  begin
    Result := 'O.k.';
    Exit;
  end;

  Result := EmptyStr;
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_CHECKSUM) <> 0 then
    Result := Result + 'Checksum,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_GND_SHORT) <> 0 then
    Result := Result + 'GroundShort,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_ID_PARITY_BIT0) <> 0 then
    Result := Result + 'IdParityBit0,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_ID_PARITY_BIT1) <> 0 then
    Result := Result + 'IdParityBit1,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_INCONSISTENT_SYNC) <> 0 then
    Result := Result + 'InconsistentSynch,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_OTHER_RESPONSE) <> 0 then
    Result := Result + 'OtherResponse,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_SLAVE_NOT_RESPONDING) <> 0 then
    Result := Result + 'SlaveNotResponding,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_SLOT_DELAY) <> 0 then
    Result := Result + 'SlotDelay,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_TIMEOUT) <> 0 then
    Result := Result + 'Timeout,';
  if (Integer(m_msg.ErrorFlags) and MSG_ERR_VBAT_SHORT) <> 0 then
    Result := Result + 'VbatShort,';
  // Remove trailing comma
  if (Result <> EmptyStr) then
    Delete(Result, Length(Result), 1);
end;

function TMessageStatus.GetProtectedIdString: String;
begin
  Result := Format('%.03Xh', [m_msg.FrameId])
end;

function TMessageStatus.GetDataString: String;
var
  i: Integer;
begin
  Result := EmptyStr;
  for i := 0 to m_msg.Length - 1 do
    Result := Result + Format('%.02X ', [m_msg.Data[i]]);
end;

procedure TMessageStatus.SetShowPeriod(value: Boolean);
begin
  if (value <> m_bShowPeriod) then
  begin
    m_bShowPeriod := value;
    m_bWasChanged := True;
  end;
end;

function TMessageStatus.GetTimeString: String;
var
  fTime: Double;
begin
  fTime := m_msg.TimeStamp;
  if m_bShowPeriod then
    fTime := (fTime - m_oldTimeStamp) / 1000;
  Result := Format('%.0f', [fTime]);
end;


{$R *.dfm}

//////////////////////////////////////////////////////////////////////////////////////////////
// Form event-handlers

procedure TMainForm.FormCreate(Sender: TObject);
begin
  InitializeControls;

  // Create the Global Frame Table and add property change handler on its items
  m_gft := TGlobalFrameTable.Create;
  // Populates FrameID combobox with global frame IDs
  UpdateFrameIds;

  // Initialize the LIN attributes
  m_hClient := 0;
  m_hHw := 0;
  m_lMask := UInt64(FRAME_FILTER_MASK);
  m_HwMode := modSlave;
  m_wBaudrate := 19200;

  // Creates the list for received messages
  m_LastMsgsList := TList.Create;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
var
  i: Integer;
begin
  // Free resources that have been initialized in FormCreate()
  m_gft.Free;
  for i := 0 to m_LastMsgsList.Count - 1 do
    TMessageStatus(m_LastMsgsList.Items[i]).Free;
  m_LastMsgsList.Clear;
  m_LastMsgsList.Free;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  // Initialize default state of components
  btnHwRefreshClick(nil);  // Refresh hardware list combobox
  rdbFilterChanged(nil);
  cbbDirection.ItemIndex := 0;
  cbbCST.ItemIndex := 1;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // Disconnect from LIN if necessary
  if (m_hClient <> 0) Then
  begin
    DoLinDisconnect;
    m_hHw := 0;
    // Unregister the application
    LIN_RemoveClient(m_hClient);
    m_hClient := 0;
  end;
end;


//////////////////////////////////////////////////////////////////////////////////////////////
// Connection event-handlers

procedure TMainForm.cbbChannelChange(Sender: TObject);
var
  hHw: HLINHW;
  mode, baudrate: Integer;
begin
  // Get the handle from the combobox item
  hHw := HLINHW(cbbChannel.Items.Objects[cbbChannel.ItemIndex]);
  if (hHw <> 0) then
  begin
    btnInit.Enabled := True;
    btnIdent.Enabled := True;
    // Read the mode of the hardware with the handle hHw (Master, Slave, or None)
    m_LastLINErr := LIN_GetHardwareParam(hHw, hwpMode, Pointer(@mode), 0);
    // Read the baudrate of the hardware with the handle hHw
    m_LastLINErr := LIN_GetHardwareParam(hHw, hwpBaudrate, Pointer(@baudrate), 0);
    // Update hardware mode combobox
    if (mode = Integer(modMaster)) then
      cbbHwMode.ItemIndex := 1
    else
      cbbHwMode.ItemIndex := 0;
    cbbHwModeChange(nil);
     // Show the current baudrate in the combobox
    if (baudrate <> 0) then
      cbbBaudrates.Text := IntToStr(baudrate)
    else
      cbbBaudrates.Text := IntToStr(m_wBaudrate);
  end
  else
  begin
    btnInit.Enabled := False;
    btnIdent.Enabled := False;
  end;
end;

procedure TMainForm.cbbHwModeChange(Sender: TObject);
begin
  // Change the "Write" button text depending on the hardware mode
  if (cbbHwMode.ItemIndex = 1) then
    btnWrite.Caption := SWriteAsMaster
  else
    btnWrite.Caption := SWriteAsSlave;
end;

procedure TMainForm.btnHwRefreshClick(Sender: TObject);
begin
  RefreshHardware;
end;

procedure TMainForm.btnIdentClick(Sender: TObject);
var
  hHw: HLINHW;
begin
  // Get hardware device from the selected combobox item
  hHw := HLINHW(cbbChannel.Items.Objects[cbbChannel.ItemIndex]);
  if (hHw <> 0) Then
  begin
    // Makes the corresponding LED on the adaptor blink
    LIN_IdentifyHardware(hHw);
  end;
end;

procedure TMainForm.btnInitClick(Sender: TObject);
begin
  if DoLinConnect then
  begin
    // Sets the connection status of the main form
    SetConnectionStatus(True);
    UpdateFrameIds;
    m_gft.UpdateLinInfo(m_hClient, m_hHw, m_HwMode, @m_lMask);
  end;
end;

procedure TMainForm.btnReleaseClick(Sender: TObject);
begin
  // Releases the current connection
  if DoLinDisconnect then
  begin
    // Stop reading timer
    tmrRead.Enabled := False;
    // Sets the connection status of the main form
    SetConnectionStatus(False);
    // Update Global Frame Table LIN information
    m_gft.UpdateLinInfo(m_hClient, m_hHw, m_HwMode, @m_lMask);
  end;
end;

//////////////////////////////////////////////////////////////////////////////////////////////
// Global frames and filter message event-handlers

procedure TMainForm.btnConfigureClick(Sender: TObject);
begin
  // Open the "Global Frame Table" dialog box
  with TGlobalFrameTableDialog.Create(Self) do
  try
    GlobalFrameTable := m_gft;
    ShowModal;
  finally
    Free;
  end;
  // Output filter information (as changes to Global Frame Table items modify it)
  btnFilterQuery.Click;
  // Update the available frame IDs (i.e. the IDs combobox in the "Write Message" UI group)
  UpdateFrameIds;
end;

procedure TMainForm.rdbFilterChanged(Sender: TObject);
begin
  // Enable/disable filter fields
  if rdbFilterCustom.Checked then
  begin
    txtFilterFrom.Enabled := True;
    txtFilterTo.Enabled := True;
    nudIdFrom.Enabled := True;
    nudIdTo.Enabled := True;
  end
  else
  begin
    txtFilterFrom.Enabled := False;
    txtFilterTo.Enabled := False;
    nudIdFrom.Enabled := False;
    nudIdTo.Enabled := False;
  end;
end;

procedure TMainForm.btnFilterApplyClick(Sender: TObject);
var
  strTemp: String;
  lMask: UInt64;
  nudFrom, nudTo: Byte;
begin
  if (m_hHw <> 0) then
  begin
    // Sets the mask according to the selected filter
    if (rdbFilterOpen.Checked = True) then
    begin
      // Set no filtering: all frames are received
      lMask := UInt64(FRAME_FILTER_MASK);
      m_LastLINErr := LIN_SetClientFilter(m_hClient, m_hHw, lMask);
      if (m_LastLINErr = errOK) then
      begin
        m_lMask := lMask;
        IncludeTextMessage('The filter was opened successfully.');
      end;
    end
    else if rdbFilterClose.Checked then
    begin
      // Filters all: all frames are denied
      lMask := 0;
      m_LastLINErr := LIN_SetClientFilter(m_hClient, m_hHw, lMask);
      if (m_LastLINErr = errOK) then
      begin
        m_lMask := lMask;
        IncludeTextMessage('The filter was closed successfully.');
      end;
    end
    else
    begin
      // Custom filtering (i.e. rdbFilterCustom.Checked): the frames for the given range will be opened
      nudFrom := Byte(nudIdFrom.Position);
      nudTo := Byte(nudIdTo.Position);
      m_LastLINErr := LIN_RegisterFrameId(m_hClient, m_hHw, nudFrom, nudTo);
      if (m_LastLINErr = errOK) then
      begin
        strTemp := Format('The filter has been customized. IDs from %d to %d will be received', [nudFrom, nudTo]);
        IncludeTextMessage(strTemp);
        m_LastLINErr := LIN_GetClientFilter(m_hClient, m_hHw, lMask);
        if (m_LastLINErr = errOK) then
          m_lMask := lMask;
      end;
    end;

    // If successful, an information message is written; if an error occurs, an error message is displayed
    if (m_LastLINErr <> errOK) then
      MessageBoxErrorShow(m_LastLINErr);
  end;
end;

procedure TMainForm.btnFilterQueryClick(Sender: TObject);
var
  strTmp: String;
  rcvMask: UInt64;
  temp: Extended;
  bit: Byte;
begin
  // Retrieves the filter corresponding to the current Client-Hardware pair
  if (m_hHw <> 0) then
  begin
    if (LIN_GetClientFilter(m_hClient, m_hHw, rcvMask) = errOK) then
    begin
      strTmp := EmptyStr;
      temp := rcvMask;
      // Convert to binary string
      for bit := 63 downto 0 do
      begin
        if (temp >= Power(2, bit)) then
        begin
          strTmp := strTmp + '1';
          temp := temp - Power(2, bit);
        end
        else
          strTmp := strTmp + '0';
      end;
      // Add message
      IncludeTextMessage(Format('The status of the filter is %.064s.', [strTmp]));
    end;
  end;
end;

procedure TMainForm.txtFilterFromExit(Sender: TObject);
var
  newValue: Integer;
begin
  // Compute new edited value
  if (txtFilterFrom.Text <> EmptyStr) then
    newValue := StrToInt(txtFilterFrom.Text)
  else
    newValue := 0;

  if (newValue > 63) then
    newValue := 63
  else if (newValue < 0) then
    newValue := 0;
  // Update Nud control
  nudIdFrom.Position := newValue;
  txtFilterFrom.Text := IntToStr(nudIdFrom.Position);
end;

procedure TMainForm.txtFilterToExit(Sender: TObject);
var
  newValue: Integer;
begin
  // Compute new edited value
  if (txtFilterTo.Text <> EmptyStr) then
    newValue := StrToInt(txtFilterTo.Text)
  else
    newValue := 0;

  if (newValue > 63) then
    newValue := 63
  else if (newValue < 0) then
    newValue := 0;
  // Update Nud control
  nudIdTo.Position := newValue;
  txtFilterTo.Text := IntToStr(nudIdTo.Position);
end;

procedure TMainForm.txtFilterKeyPress(Sender: TObject; var Key: Char);
begin
  // Convert the character to its upper-case equivalent
  Key := (UpperCase(Key))[1];

  // The key is the Delete (Backspace) Key
  if (Key = #8) then
    Exit;
  // The key is a number between 0-9
  if ((Key >= '0') and (Key <= '9')) then
    Exit;
  // Key is not a number
  Key := #0;
end;

//////////////////////////////////////////////////////////////////////////////////////////////
// Read message event-handlers

procedure TMainForm.rdbTimerChanged(Sender: TObject);
begin
  // According to the kind of reading, a timer or a button will be enabled
  if (btnRelease.Enabled and rdbTimer.Checked) then
    tmrRead.Enabled := True
  else
    tmrRead.Enabled := False;

  btnRead.Enabled := not rdbTimer.Checked and btnRelease.Enabled;
end;

procedure TMainForm.btnMsgClearClick(Sender: TObject);
begin
  // Clear information shown in the messages ListView
  m_LastMsgsList.Clear;
  lstMessages.Clear;
end;

procedure TMainForm.btnReadClick(Sender: TObject);
begin
  ReadMessages;
end;

procedure TMainForm.chbShowPeriodClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to m_LastMsgsList.Count - 1 do
    TMessageStatus(m_LastMsgsList.Items[i]).ShowPeriod := chbShowPeriod.Checked;
end;

//////////////////////////////////////////////////////////////////////////////////////////////
// Write message event-handlers

procedure TMainForm.cbbIDChange(Sender: TObject);
var
  frame: TFrameDefinition;
begin
  frame := TFrameDefinition(cbbID.Items.Objects[cbbID.ItemIndex]);
  // Check if a global frame is selected
  if (frame <> nil) then
  begin
    // Update components according to the selected frame informations
    txtID.Text := frame.ProtectedIdAsString;
    txtLength.Text := frame.LengthAsString;
    nudLength.Position := frame.Length;

    case frame.Direction of
      dirDisabled:
        cbbDirection.ItemIndex := 0;
      dirPublisher:
        cbbDirection.ItemIndex := 1;
      dirSubscriber:
        cbbDirection.ItemIndex := 2;
      dirSubscriberAutoLength:
        cbbDirection.ItemIndex := 3;
    end;

    case frame.ChecksumType of
      cstAuto:
        cbbCST.ItemIndex := 0;
      cstClassic:
        cbbCST.ItemIndex := 1;
      cstEnhanced:
        cbbCST.ItemIndex := 2;
      cstCustom:
        cbbCST.ItemIndex := 3;
    end;
    // Force update on data fields (both Length and Direction impact on Enabled state of these components)
    // if length value is not changed but direction switch from subscriber to publisher
    // data textfield would not be disabled
    nudLengthClick(nil, btNext);
  end;
end;

procedure TMainForm.nudLengthClick(Sender: TObject; Button: TUDBtnType);
begin
  txtData0.Enabled := False;
  txtData1.Enabled := False;
  txtData2.Enabled := False;
  txtData3.Enabled := False;
  txtData4.Enabled := False;
  txtData5.Enabled := False;
  txtData6.Enabled := False;
  txtData7.Enabled := False;
  // Only Publisher direction allows editing of Data textboxes
  if (cbbDirection.ItemIndex <> 1) then
      Exit;
  // Enable the same number of data textbox as the nudLength value
  case nudLength.Position of
    1:
      begin
        txtData0.Enabled := True;
      end;
    2:
      begin
        txtData0.Enabled := True;
        txtData1.Enabled := True;
      end;
    3:
      begin
        txtData0.Enabled := True;
        txtData1.Enabled := True;
        txtData2.Enabled := True;
      end;
    4:
      begin
        txtData0.Enabled := True;
        txtData1.Enabled := True;
        txtData2.Enabled := True;
        txtData3.Enabled := True;
      end;
    5:
      begin
        txtData0.Enabled := True;
        txtData1.Enabled := True;
        txtData2.Enabled := True;
        txtData3.Enabled := True;
        txtData4.Enabled := True;
      end;
    6:
      begin
        txtData0.Enabled := True;
        txtData1.Enabled := True;
        txtData2.Enabled := True;
        txtData3.Enabled := True;
        txtData4.Enabled := True;
        txtData5.Enabled := True;
      end;
    7:
      begin
        txtData0.Enabled := True;
        txtData1.Enabled := True;
        txtData2.Enabled := True;
        txtData3.Enabled := True;
        txtData4.Enabled := True;
        txtData5.Enabled := True;
        txtData6.Enabled := True;
      end;
    8:
      begin
        txtData0.Enabled := True;
        txtData1.Enabled := True;
        txtData2.Enabled := True;
        txtData3.Enabled := True;
        txtData4.Enabled := True;
        txtData5.Enabled := True;
        txtData6.Enabled := True;
        txtData7.Enabled := True;
      end;
  end;
end;

procedure TMainForm.txtData0Exit(Sender: TObject);
var
  CurrentEdit: TEdit;
begin
  // All the Textbox Data fields are represented with 2 characters.
  // Therefore if the Length of the text is smaller than 2, we add
  // a "0"
  if Sender is TEdit then
  begin
    CurrentEdit := TEdit(Sender);
    while (Length(CurrentEdit.Text) <> 2) do
      CurrentEdit.Text := ('0' + CurrentEdit.Text);
  end;
end;

procedure TMainForm.txtData0KeyPress(Sender: TObject; var Key: Char);
begin
  // We convert the character to its upper-case equivalent
  Key := (UpperCase(Key))[1];

  // The key is the Delete (Backspace) Key
  if (Ord(Key) = 8) then
    Exit;
  // The key is a number between 0-9
  if ((Ord(Key) > 47) and (Ord(Key) < 58)) then
    Exit;
  // The Key is a character between A(a) and F(f)
  if ((Ord(Key) > 64) and (Ord(Key) < 71)) then
    Exit;

  // Key is neither a number nor a character between A(a) and F(f)
  Key := #0;
end;

procedure TMainForm.btnWriteClick(Sender: TObject);
var
  msg: TLINMsg;
  frame: TFrameDefinition;
begin
  // Assert that a frame ID has been selected
  if (cbbID.ItemIndex = -1) then
  begin
    cbbID.SetFocus;
    Exit;
  end;

  // Get the TFrameDefinition object associated to the selected frame ID
  frame := TFrameDefinition(cbbID.Items.Objects[cbbID.ItemIndex]);

  // Create a new LIN frame message and copy the data
  msg.FrameId := frame.ProtectedId;
  msg.Direction := TLINDirection(cbbDirection.Items.Objects[cbbDirection.ItemIndex]);
  msg.ChecksumType := TLINChecksumType(cbbCST.Items.Objects[cbbCST.ItemIndex]);
  msg.Length := Byte(nudLength.Position);
  // Fill data array
  msg.Data[0] := Byte(StrToInt('0x' + txtData0.Text));
  msg.Data[1] := Byte(StrToInt('0x' + txtData1.Text));
  msg.Data[2] := Byte(StrToInt('0x' + txtData2.Text));
  msg.Data[3] := Byte(StrToInt('0x' + txtData3.Text));
  msg.Data[4] := Byte(StrToInt('0x' + txtData4.Text));
  msg.Data[5] := Byte(StrToInt('0x' + txtData5.Text));
  msg.Data[6] := Byte(StrToInt('0x' + txtData6.Text));
  msg.Data[7] := Byte(StrToInt('0x' + txtData7.Text));

  // Check if the hardware is initialized as Master
  if (m_HwMode = modMaster) then
  begin
    // Calculate the checksum for the message
    LIN_CalculateChecksum(msg);
    // Try to send the LIN frame with LIN_Write()
    m_LastLINErr := LIN_Write(m_hClient, m_hHw, msg);
  end
  else
  begin
    // If the hardare is initialized as Slave, only update the data in the LIN frame
    m_LastLINErr := LIN_UpdateByteArray(m_hClient, m_hHw, frame.Id, 0, msg.Length, @msg.Data);
  end;
  // Show error if any
  if (m_LastLINErr <> errOK) then
    MessageBoxErrorShow(m_LastLINErr);
end;


//////////////////////////////////////////////////////////////////////////////////////////////
// Information group event-handlers

procedure TMainForm.btnGetVersionsClick(Sender: TObject);
var
  lpVersion: TLINVersion;
  buffer: array[0..255] of AnsiChar;
  info, strTemp: String;
  iPos: Integer;
begin
  // We get the vesion of the PLIN-API
  m_LastLINErr := LIN_GetVersion(lpVersion);
  if (m_LastLINErr = errOK) then
  begin
    strTemp := Format('API Version: %d.%d.%d.%d', [lpVersion.Major, lpVersion.Minor,
      lpVersion.Build, lpVersion.Revision]);
    IncludeTextMessage(strTemp);
    // We get the driver version
    m_LastLINErr := LIN_GetVersionInfo(buffer, 255);
    if (m_LastLINErr = errOK) then
    begin
      IncludeTextMessage('Channel/Driver Version: ');

      // Because this information contains line control characters (several lines)
      // we split this also in several entries in the Information listbox
      info := String(buffer);
      while (info <> EmptyStr) do
      begin
        iPos := Pos(#$A, info);
        if(iPos = 0) then
            iPos := Length(info);
        IncludeTextMessage('     * ' + Copy(info, 1, iPos));
        Delete(info, 1, iPos);
      end;
    end;
  end;
  // If an error occurred, a message is shown
  if (m_LastLINErr <> errOK) then
    MessageBoxErrorShow(m_LastLINErr);
end;

procedure TMainForm.btnInfoClearClick(Sender: TObject);
begin
  // The information contained in the Information listbox is cleared
  lbxInfo.Clear;
end;

procedure TMainForm.btnResetClick(Sender: TObject);
begin
  // Flushes the Receive Queue of the Client and resets its counters
  m_LastLINErr := LIN_ResetClient(m_hClient);

  // If it fails, an error message is shown
  if (m_LastLINErr <> errOK) then
    MessageBoxErrorShow(m_LastLINErr)
  else
  begin
    // Clears the message listview
    btnMsgClear.Click;
    IncludeTextMessage('Receive queue and counters successfully reset');
  end;
end;

procedure TMainForm.btnStatusClick(Sender: TObject);
var
  lStatus: TLINHardwareStatus;
begin
  // Retrieves the status of the LIN Bus and outputs its state in the information listView
  m_LastLINErr := LIN_GetStatus(m_hHw, lStatus);
  if (m_LastLINErr = errOK) then
  begin
    case lStatus.Status of
      hwsNotInitialized:
        IncludeTextMessage('Hardware: Not Initialized');
      hwsAutobaudrate:
        IncludeTextMessage('Hardware: Baudrate Detection');
      hwsActive:
        IncludeTextMessage('Bus: Active');
      hwsSleep:
        IncludeTextMessage('Bus: Sleep');
      hwsShortGround:
        IncludeTextMessage('Bus-Line: Shorted Ground');
      hwsVBatMissing:
        IncludeTextMessage('Hardware: Vbat Missing');
    end;
  end
  else
    MessageBoxErrorShow(m_LastLINErr);
end;


//////////////////////////////////////////////////////////////////////////////////////////////
// Timer event-handler & functions

procedure TMainForm.tmrDisplayTimer(Sender: TObject);
begin
  // Update display of received messages
  DisplayMessages;
end;

procedure TMainForm.tmrReadTimer(Sender: TObject);
begin
  // Checks if in the receive-queue are currently messages for read
  ReadMessages;
end;


//////////////////////////////////////////////////////////////////////////////////////////////
// LIN connection functions

procedure TMainForm.RefreshHardware;
var
  hwHandles: array of HLINHW;
  buffSize: Word;
  i, count, hwType, devNo, channel, mode: Integer;
  err: TLINError;
  hHw: HLINHW;
  str: String;
begin
  // Get the required buffer length
  count := 0;
  LIN_GetAvailableHardware(nil, 0, count);
  if (count = 0) Then  // Use default value if either no hw is connected or an unexpected error occurred
    count := 16;
  SetLength(hwHandles, count);
  buffSize := count * SizeOf(HLINHW);

  // Get all available LIN hardware.
  err := LIN_GetAvailableHardware(PHLINHW(hwHandles), buffSize, count);
  if (err = errOK) Then
  begin
    cbbChannel.Items.Clear;
    // If no error occurs
    if (count = 0) Then
    begin
      // No LIN hardware was found, show an empty entry
      hHw := 0;
      cbbChannel.Items.AddObject(SHardwareNothingFound, TObject(hHw));
    end
    else
    begin
      // For each found LIN hardware
      for i := 0 to count - 1 do
      begin
        // Get the handle of the hardware
        hHw := hwHandles[i];
        // Read the type of the hardware with the handle hHw
        LIN_GetHardwareParam(hHw, hwpType, Pointer(@hwType), 0);
        // Read the device number of the hardware with the handle hHw
        LIN_GetHardwareParam(hHw, hwpDeviceNumber, Pointer(@devNo), 0);
        // Read the channel number of the hardware with the handle hHw
        LIN_GetHardwareParam(hHw, hwpChannelNumber, Pointer(@channel), 0);
        // Read the mode of the hardware with the handle hHw (Master, Slave or None)
        LIN_GetHardwareParam(hHw, hwpMode, Pointer(@mode), 0);

        // Create a ComboBox item
        // If the hardware type is a known hardware show the name in the label of the entry
        case hwType of
          LIN_HW_TYPE_USB_PRO:
            str := SHardwareTypeLIN;
          LIN_HW_TYPE_USB_PRO_FD:
            str := SHardwareTypeLINFD;
          LIN_HW_TYPE_PLIN_USB:
            str := SHardwareTypePLIN;
          else
            str := SHardwareTypeUnkown;
        end;

        str := Format('%s - dev. %d, chan. %d', [str, devNo, channel]);
        cbbChannel.Items.AddObject(str, TObject(hHw));
      end;
    end;
    cbbChannel.ItemIndex := 0;
    cbbChannelChange(cbbChannel);   // Call selection changed handler
  end
  else
    MessageBoxErrorShow(m_LastLINErr);
end;

function TMainForm.DoLinConnect: Boolean;
var
  hHw: HLINHW;
  baudrate: Word;
  currentBaudrate: Integer;
  mode: TLINHardwareMode;
begin
  // Initialization
  Result := False;

  if (m_hHw <> 0) then
  begin
    // If a connection to hardware already exits, disconnect this connection first
    if not DoLinDisconnect then
      Exit;
  end;
  // Get the selected Hardware handle from the combobox item
  hHw := HLINHW(cbbChannel.Items.Objects[cbbChannel.ItemIndex]);
  if (hHw <> 0) then
  begin
    if (m_hClient = 0) then
      // Register this application as a LIN client
      m_LastLINErr := LIN_RegisterClient(PAnsiChar(SPLinClientName), Handle, m_hClient);
    // The local hardware handle is valid, get the current mode of the hardware
    LIN_GetHardwareParam(hHw, hwpMode, Pointer(@mode), 0);
    // Get the current baudrate of the hardware
    LIN_GetHardwareParam(hHw, hwpBaudrate, Pointer(@currentBaudrate), 0);
    // Try to connect the application client to the hardware with the local handle
    m_LastLINErr := LIN_ConnectClient(m_hClient, hHw);
    if (m_LastLINErr = errOK) then
    begin
      // If the connection is successful, assign the local handle to the member variable
      m_hHw := hHw;
      // Get the selected hardware mode
      if (cbbHwMode.ItemIndex = 1) then
        mode := modMaster
      else
        mode := modSlave;
      // Get the selected baudrate
      baudrate := StrToIntDef(cbbBaudrates.Text, 0);
      // Get the selected hardware channel
      if ((mode = modNone) or (Word(currentBaudrate) <> baudrate)) then
      begin
        // If the current hardware is not initialized try to intialize the hardware with mode and baudrate
        m_LastLINErr := LIN_InitializeHardware(m_hClient, m_hHw, mode, baudrate)
      end;
      if (m_LastLINErr = errOK) then
      begin
        // Assign the Hardware Mode to member variable
        m_HwMode := mode;
        // Assign the baudrate index to member variable
        m_wBaudrate := baudrate;
        // Set the client filter with the mask
        m_LastLINErr := LIN_SetClientFilter(m_hClient, m_hHw, m_lMask);
        // Read the frame table from the connected hardware
        ReadFrameTableFromHw;
        // Reset the last LIN error code to default
        m_LastLINErr := errOK;
        Result := True;
      end
      else
      begin
        // An error occurred while initializing hardware,
        // set the member variable to default
        m_hHw := 0;
      end;
    end
    else
    begin
      // The local hardware handle is invalid and/or an error occurs while connecting
      // hardware with client, set the member variable to default
      m_hHw := 0;
    end;

    // Check if any LIN error occurred
    if (m_LastLINErr <> errOK) Then
      MessageBoxErrorShow(m_LastLINErr);
  end
  else
    m_hHw := 0;
end;

function TMainForm.DoLinDisconnect: Boolean;
var
  otherClient: Boolean;
  ownClient: Boolean;
  hClients: array[0..254] of Byte;
  i: Integer;
begin
  Result := False;

  // If the application was registered with LIN as client before
  if (m_hHw <> 0) then
  begin
    // The client is already connected to a LIN hardware.
    // Before disconnecting from the hardware, check
    // the connected clients and determine whether the
    // hardware configuration has to be reset or not

    otherClient := False;
    ownClient := False;
    
    // Get the connected clients from the LIN hardware
    m_LastLINErr := LIN_GetHardwareParam(m_hHw, hwpConnectedClients, Pointer(@hClients), Length(hClients));
    if (m_LastLINErr = errOK) then
    begin
      // No errors, check all client handles
      for i := 0 to Length(hClients) - 1 do
      begin
        // If client handle is invalid
        if (hClients[i] = 0) then
          Continue;
        // Set the boolean to True if the handle isn't the handle of this application.
        // When the boolean is already True it can never be set to False.
        otherClient := otherClient or (hClients[i] <> m_hClient);
        // Set the boolean to True if the handle is the handle of this application.
        // When the boolean is already True it can never set to False.
        ownClient := ownClient or (hClients[i] = m_hClient);
      end;
    end;
    // If another application is also connected to the LIN hardware do not reset the configuration
    if not otherClient then
    begin
      // No other application connected, reset the configuration of the LIN hardware
      LIN_ResetHardwareConfig(m_hClient, m_hHw);
    end;
    // If this application is connected to the hardware then disconnect the client
    if ownClient then
    begin
      m_LastLINErr := LIN_DisconnectClient(m_hClient, m_hHw);
      if (m_LastLINErr = errOK) then
      begin
        m_hHw := 0;
        Result := True;
      end
      else
      begin
        // Error while disconnecting from hardware
        MessageBoxErrorShow(m_LastLINErr);
      end;
    end;
  end
  else
    Result := True;
end;

procedure TMainForm.ReadFrameTableFromHw;
var
  i: Integer;
  frameEntry: TLINFrameEntry;
  err: TLINError;
  mask: UInt64;
begin
  // Initialize the member variable for the client mask with 0
  m_lMask := 0;
  // Iterate the Global Frame Table
  for i := 0 to m_gft.Count - 1 do
  begin
    // Before a frame entry can be read from the hardware, the frame ID
    // of the wanted entry must be set
    frameEntry.FrameId := m_gft[i].Id;
    // Read the information of the specified frame entry from the hardware
    err := LIN_GetFrameEntry(m_hHw, frameEntry);
    // Check the result value of the LIN-API function call
    if (err = errOK) then
    begin
      // LIN-API function call successful, copy the frame entry information
      // to the TFrameDefinition object
      m_gft[i].setLength(frameEntry.Length);
      m_gft[i].setDirection(frameEntry.Direction);
      m_gft[i].setChecksumType(frameEntry.ChecksumType);
      if (m_gft[i].Direction <> dirDisabled) then
      begin
        // If the direction is not disabled then set the bit in the client
        // filter mask
        mask := (UInt64(1) shl i) And FRAME_FILTER_MASK;
        m_lMask := m_lMask Or mask;
      end;
    end;
  end;
  // If the Client and Hardware handles are valid
  if ((m_hClient <> 0) and (m_hHw <> 0)) then
      // Set the client filter
      LIN_SetClientFilter(m_hClient, m_hHw, m_lMask);
  // Updates the displayed frame IDs
  UpdateFrameIds;
end;


///////////////////////////////////////////////////////////////////////////
// LIN Message processing functions

procedure TMainForm.ReadMessages;
var
  msg: TLINRcvMsg;
begin
  // We read at least one time the queue looking for messages.
  // If a message is found, we look again trying to find more.
  // If the queue is empty or an error occurs, we exit the
  // repeat-until loop
  repeat
    m_LastLINErr := LIN_Read(m_hClient, msg);
    // If at least one frame is received by the LIN-API,
    // check if the received frame has the standard type, in which
    // case it is ignored
    if (m_LastLINErr = errOK) and (msg.MsgType = mstStandard) then
      ProcessMessage(msg);
  until (not btnRelease.Enabled) or (Integer(m_LastLINErr) and Integer(errRcvQueueEmpty) <> 0);
end;

procedure TMainForm.ProcessMessage(const linMsg: TLINRcvMsg);
var
  i: Integer;
  msg: TMessageStatus;
begin
  // We search if a message (Same ID and Type) has already been received or
  // if this is a new message
  for i := 0 to m_LastMsgsList.Count - 1 do
  begin
    msg := TMessageStatus(m_LastMsgsList.Items[i]);
    if (msg.LINMsg.FrameId = linMsg.FrameId) then
    begin
      // Modify the message and exit
      msg.Update(linMsg);
      Exit;
    end;
  end;
  // Message not found, it will be created
  InsertMsgEntry(linMsg);
end;

procedure TMainForm.InsertMsgEntry(const newMsg: TLINRcvMsg);
var
  i: Integer;
  msg: TMessageStatus;
  strId: String;
  item: TListItem;
begin
  // We add this status in the last message list
  msg := TMessageStatus.Create(newMsg, lstMessages.Items.Count);
  m_LastMsgsList.Add(msg);

  // Search and retrieve the ID in the global frame table associated with the frame Protected-ID
  strId := EmptyStr;
  for i := 0 to m_gft.Count - 1 do
  begin
    if (msg.LINMsg.FrameId = m_gft[i].ProtectedId) then
    begin
      strId := m_gft[i].IdAsString;
      Break;
    end;
  end;

  // Add the new ListView item with the ID of the message
  item := lstMessages.Items.Add;
  item.Caption := strId;
  // We set the length of the message
  item.SubItems.Add(IntToStr(msg.LINMsg.Length));
  // We set the data of the message
  item.SubItems.Add(msg.DataString);
  // We set the message count message (this is the first, so count is 1)
  item.SubItems.Add(IntToStr(msg.Count));
  // Add time stamp information if needed
  item.SubItems.Add(msg.TimeString);
  // We set the direction of the message
  item.SubItems.Add(msg.DirectionString);
  // We set the error of the message
  item.SubItems.Add(msg.ErrorString);
  // We set the CST of the message
  item.SubItems.Add(msg.CSTString);
  // We set the CRC of the message
  item.SubItems.Add(msg.ChecksumString);
end;

procedure TMainForm.DisplayMessages;
var
  i: Integer;
  msg: TMessageStatus;
  item: TListItem;
begin
  for i := 0 to m_LastMsgsList.Count - 1 do
  begin
    msg := TMessageStatus(m_LastMsgsList.Items[i]);
    if msg.WasChanged then
    begin
      item := lstMessages.Items[i];
      // Update the length of the message
      item.SubItems[0] := IntToStr(msg.LINMsg.Length);
      // Update the data of the message
      item.SubItems[1] := msg.DataString;
      // Update the message count message
      item.SubItems[2] := IntToStr(msg.Count);
      // Update time stamp information
      item.SubItems[3] := msg.TimeString;
      // Update the direction of the message
      item.SubItems[4] := msg.DirectionString;
      // Update the error of the message
      item.SubItems[5] := msg.ErrorString;
      //  Update the CST of the message
      item.SubItems[6] := msg.CSTString;
      // Update the CRC of the message
      item.SubItems[7] := msg.ChecksumString;

      msg.WasChanged := False;
    end;
  end;
end;


///////////////////////////////////////////////////////////////////////////
// Helper functions

procedure TMainForm.InitializeControls;
begin
  // Populates Direction combobox
  cbbDirection.Items.Clear;
  cbbDirection.Items.AddObject(SLinDirectionDisabled, TObject(dirDisabled));
  cbbDirection.Items.AddObject(SLinDirectionPublisher, TObject(dirPublisher));
  cbbDirection.Items.AddObject(SLinDirectionSubscriber, TObject(dirSubscriber));
  cbbDirection.Items.AddObject(SLinDirectionAuto, TObject(dirSubscriberAutoLength));
  // Populates ChecksumType combobox
  cbbCST.Items.Clear;
  cbbCST.Items.AddObject(SLinCSTAuto, TObject(cstAuto));
  cbbCST.Items.AddObject(SLinCSTClassic, TObject(cstClassic));
  cbbCST.Items.AddObject(SLinCSTEnhanced, TObject(cstEnhanced));
  cbbCST.Items.AddObject(SLinCSTCustom, TObject(cstCustom));
end;

procedure TMainForm.UpdateFrameIds;
var
  frame, selectedFrame: TFrameDefinition;
  i: Integer;
begin
  selectedFrame := nil;
  // Gets the selected ID if it exists before clearing combobox
  if (cbbID.ItemIndex >= 0) then
    selectedFrame := TFrameDefinition(cbbID.Items.Objects[cbbID.ItemIndex]);

  // Clears and populates FrameID combobox with global frame IDs
  cbbID.Clear;
  for i := 0 to m_gft.Count - 1 do
  begin
    frame := m_gft[i];
    // Add only frames that are not disabled
    if (frame.Direction = dirDisabled) then
      Continue;
    // Add frame and data
    cbbID.AddItem(frame.IdAsString, TObject(frame));
    // Check if the new item was selected before the update
    if (selectedFrame = frame) then
      cbbID.ItemIndex := cbbID.Items.Count - 1;
  end;
end;

procedure TMainForm.IncludeTextMessage(strMsg: String);
begin
  lbxInfo.Items.Add(strMsg);
  lbxInfo.ItemIndex := lbxInfo.Items.Count - 1;
end;

procedure TMainForm.SetConnectionStatus(bConnected: Boolean);
begin
  // Buttons
  btnInit.Enabled := Not bConnected;
  btnConfigure.Enabled := bConnected;
  btnRead.Enabled := bConnected And rdbManual.Checked;
  btnWrite.Enabled := bConnected;
  btnRelease.Enabled := bConnected;
  btnFilterApply.Enabled := bConnected;
  btnFilterQuery.Enabled := bConnected;
  btnGetVersions.Enabled := bConnected;
  btnHwRefresh.Enabled := Not bConnected;
  btnStatus.Enabled := bConnected;
  btnReset.Enabled := bConnected;

  // ComboBoxes
  cbbBaudrates.Enabled := Not bConnected;
  cbbChannel.Enabled := Not bConnected;
  cbbHwMode.Enabled := Not bConnected;

  // Hardware configuration and read mode
  if (not bConnected) then
    cbbChannelChange(nil)
  else
    rdbTimerChanged(nil);

  // Display messages in grid
  tmrDisplay.Enabled := bConnected;
end;

function TMainForm.GetFormatedError(error: TLINError): String;
var
  buffer: array[0..255] of Ansichar;
begin
  Result := EmptyStr;
  // If any error occurred, display the error text in a message box
  // $00 = Neutral
  // $07 = Language German
  // $09 = Language English
  if (LIN_GetErrorText(error, $09, buffer, 255) <> errOK) then
    Result := Format('An error occurred. Errorcode''s text (%d) couldn''t be retrieved', [Integer(error)])
  else
    Result := String(buffer);
end;

procedure TMainForm.MessageBoxErrorShow(error: TLINError);
begin
  MessageDlg(GetFormatedError(error), mtError, [mbOk], 0);
end;

end.
