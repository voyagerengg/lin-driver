unit FrameDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ValEdit, ComCtrls,
  PLinApi, GlobalFrameTable;
  
type
  TGlobalFrameTableDialog = class(TForm)
    Label1: TLabel;
    lvGFT: TListView;
    Label2: TLabel;
    pgGFTDef: TValueListEditor;
    btnClose: TButton;
    procedure lvGFTSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure pgGFTDefStringsChange(Sender: TObject);
    procedure lvGFTKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    // Global Frame Table object that holds all frame definitions
    m_gft: TGlobalFrameTable;
    // Boolean to register the current edit mode of different controls
    // To protect the control's event for recursive calling
    m_fAutomatic: Boolean;
    // Stores the frames selected in the 'Global Frame Table' ListView control
    m_SelectedFrames: TList;
    // Fills the ListView 'Global Frame Table' with the data from TGlobalFrameTable
    procedure FillListView;
    // Updates the property grid with the selected frames from m_SelectedFrames
    procedure UpdatePropertyGrid;
  public
    property GlobalFrameTable: TGlobalFrameTable read m_gft write m_gft;
  end;

implementation

{$R *.dfm}


const
  propChecksumType    = 'Checksum Type';
  propDirection       = 'Direction Type';
  propLength          = 'Length';
  propID              = 'ID';
  propPID             = 'Protected ID';

procedure TGlobalFrameTableDialog.FormCreate(Sender: TObject);
begin
  m_SelectedFrames := TList.Create;
end;

procedure TGlobalFrameTableDialog.FormShow(Sender: TObject);
var
  i, j: Integer;
begin
  // Lock updates to prevent unnecessary events
  m_fAutomatic := true;
  try
    FillListView;
    // Changeable properties: Checksum Type
    i := pgGFTDef.Strings.Add(propChecksumType + '=');
    pgGFTDef.ItemProps[i].EditStyle := esPickList;
    pgGFTDef.ItemProps[i].ReadOnly := False;
    pgGFTDef.ItemProps[i].PickList.AddObject(ChecksumTypeToString(cstAuto), TObject(cstAuto));
    pgGFTDef.ItemProps[i].PickList.AddObject(ChecksumTypeToString(cstClassic), TObject(cstClassic));
    pgGFTDef.ItemProps[i].PickList.AddObject(ChecksumTypeToString(cstEnhanced), TObject(cstEnhanced));
    pgGFTDef.ItemProps[i].PickList.AddObject(ChecksumTypeToString(cstCustom), TObject(cstCustom));
    // Changeable properties: Direction
    i := pgGFTDef.Strings.Add(propDirection + '=');
    pgGFTDef.ItemProps[i].EditStyle := esPickList;
    pgGFTDef.ItemProps[i].ReadOnly := False;
    pgGFTDef.ItemProps[i].PickList.AddObject(DirectionToString(dirDisabled), TObject(dirDisabled));
    pgGFTDef.ItemProps[i].PickList.AddObject(DirectionToString(dirPublisher), TObject(dirPublisher));
    pgGFTDef.ItemProps[i].PickList.AddObject(DirectionToString(dirSubscriber), TObject(dirSubscriber));
    pgGFTDef.ItemProps[i].PickList.AddObject(DirectionToString(dirSubscriberAutoLength), TObject(dirSubscriberAutoLength));
    // Changeable properties: Length
    i := pgGFTDef.Strings.Add(propLength + '=');
    pgGFTDef.ItemProps[i].EditStyle := esPickList;
    pgGFTDef.ItemProps[i].ReadOnly := False;
    for j := 1 to 8 do
      pgGFTDef.ItemProps[i].PickList.AddObject(IntToStr(j), TObject(j));
    // Read only properties: ID
    i := pgGFTDef.Strings.Add(propID + '=');
    pgGFTDef.ItemProps[i].EditStyle := esSimple;
    pgGFTDef.ItemProps[i].ReadOnly := True;
    // Read only properties: Protected ID
    i := pgGFTDef.Strings.Add(propPID + '=');
    pgGFTDef.ItemProps[i].EditStyle := esSimple;
    pgGFTDef.ItemProps[i].ReadOnly := True;
  finally
    // Remove lock
    m_fAutomatic := False;
  end;
end;

procedure TGlobalFrameTableDialog.FormDestroy(Sender: TObject);
begin
  m_SelectedFrames.Free;
end;

procedure TGlobalFrameTableDialog.FillListView;
var
  i: Integer;
  CurrentItem: TListItem;
  frame: TFrameDefinition;
begin
  // Lock the ListView.
  lvGFT.Items.BeginUpdate;
  try
    // Clear the ListView that will show the Frame Definition of the
    // Global Frame Table
    lvGFT.Items.Clear;
    // Add every object, Frame Definition, from the Global Frame Table
    // to the ListView
    for i := 0 to m_gft.Count - 1 do
    begin
      frame := m_gft[i];
      // Add the new ListView item with the type of the message
      CurrentItem := lvGFT.Items.Add;
      CurrentItem.Caption := frame.IdAsString;
      CurrentItem.SubItems.Add(frame.ProtectedIdAsString);
      CurrentItem.SubItems.Add(frame.DirectionAsString);
      CurrentItem.SubItems.Add(frame.LengthAsString);
      CurrentItem.SubItems.Add(frame.ChecksumTypeAsString);
    end;
  finally
    // Unlock and update the ListView now
    lvGFT.Items.EndUpdate;
  end;
end;

procedure TGlobalFrameTableDialog.UpdatePropertyGrid;
var
  frame: TFrameDefinition;
  direction: TLINDirection ;
  checksumType: TLINChecksumType;
  bSameDirection, bSameChecksumType, bSameLength: Boolean;
  len: Integer;
  i: Integer;
begin
  if (m_SelectedFrames.Count = 0) then
  begin
    // Reset "property grid" values
    pgGFTDef.Values[propChecksumType] := EmptyStr;
    pgGFTDef.Values[propDirection] := EmptyStr;
    pgGFTDef.Values[propLength] := EmptyStr;
    pgGFTDef.Values[propID] := EmptyStr;
    pgGFTDef.Values[propPID] := EmptyStr;
  end
  else if (m_SelectedFrames.Count = 1) then
  begin
    // Fill "property grid" with selected frame values
    frame := TFrameDefinition(m_SelectedFrames.Items[0]);
    pgGFTDef.Values[propChecksumType] := frame.ChecksumTypeAsString;
    pgGFTDef.Values[propDirection] :=frame.DirectionAsString;
    pgGFTDef.Values[propLength] := frame.LengthAsString;
    pgGFTDef.Values[propID] := frame.IdAsString;
    pgGFTDef.Values[propPID] := frame.ProtectedIdAsString;
  end
  else
  begin
    // Get the first selected frame
    frame := TFrameDefinition(m_SelectedFrames.Items[0]);
    // Get its properties
    checksumType := frame.ChecksumType;
    direction := frame.Direction;
    len := frame.Length;
    // We have to loop through all selected frames to
    // search for identical properties in the selected frames.
    // If values for a property are different a blank value is displayed
    bSameDirection := True;
    bSameChecksumType := True;
    bSameLength := True;
    for i := 0 to m_SelectedFrames.Count - 1 do
    begin
      frame := TFrameDefinition(m_SelectedFrames.Items[i]);
      if (checksumType <> frame.ChecksumType) then
        bSameChecksumType := False;
      if (direction <> frame.Direction) then
        bSameDirection := False;
      if (len <> frame.Length) then
        bSameLength := False;
    end;
    frame := TFrameDefinition(m_SelectedFrames.Items[0]);
    // If all the frames have the same CST, display it otherwise reset to the original blank value
    if bSameChecksumType then
      pgGFTDef.Values[propChecksumType] := frame.ChecksumTypeAsString
    else
      pgGFTDef.Values[propChecksumType] := EmptyStr;
    // If all the frames have the same direction, display it otherwise reset to the original blank value
    if bSameDirection then
      pgGFTDef.Values[propDirection] := frame.DirectionAsString
    else
      pgGFTDef.Values[propDirection] := EmptyStr;
    // If all the frames have the same length, display it otherwise reset to the original blank value
    if bSameLength then
      pgGFTDef.Values[propLength] := frame.LengthAsString
    else
      pgGFTDef.Values[propLength] := EmptyStr;
    // These properties are always different, reset to the original blank value
    pgGFTDef.Values[propID] := EmptyStr;
    pgGFTDef.Values[propPID] := EmptyStr;
  end;
end;

procedure TGlobalFrameTableDialog.lvGFTKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  // Select all items
  if (Key = Ord('A')) and (ssCtrl in Shift) then
  begin
    (Sender as TListView).SelectAll;
    Key := 0;
  end;
end;

procedure TGlobalFrameTableDialog.lvGFTSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var
  nIdx : Integer;
  curItem: TListItem;
begin
  // Avoid internal calls
  if not m_fAutomatic then
  begin
    m_fAutomatic := True;
    try
      // Clear the selected frames list
      m_SelectedFrames.Clear;
      // Get the count of selected elements in the ListView 'lvGFT'.
      // Each element is associated with an element in the TGlobalFrameTable object
      curItem := lvGFT.Selected;
      if Assigned(curItem) then
      begin
        // Add all items to the internal item selection list
        nIdx := lvGFT.Items.IndexOf(curItem);
        m_SelectedFrames.Add(m_gft[nIdx]);
        curItem := lvGFT.GetNextItem(curItem, sdAll, [isSelected]);
        // If there are more than one
        while Assigned(curItem) do
        begin
          nIdx := lvGFT.Items.IndexOf(curItem);
          m_SelectedFrames.Add(m_gft[nIdx]);
          curItem := lvGFT.GetNextItem(curItem, sdAll, [isSelected]);
        end;
      end;
      // Update the PropertyGrid control with the newly selected frames
      UpdatePropertyGrid;
    finally
      // Remove lock
      m_fAutomatic := False;
    end;
  end;
end;

procedure TGlobalFrameTableDialog.pgGFTDefStringsChange(Sender: TObject);
var
  value: String;
  direction: TLINDirection;
  checksumType: TLINChecksumType;
  length, i: Integer;
  frame: TFrameDefinition;
  CurrentItem: TListItem;
  setLen, setCST, setDir: Boolean;
begin
  // Avoid internal calls
  if not m_fAutomatic then
  begin
    m_fAutomatic := True;
    try
      // If no frame is selected, only update propertyGrid to display original/blank values and return
      if (m_SelectedFrames.Count = 0) then
      begin
        // Update and return
        UpdatePropertyGrid;
        Exit;
      end;

      // Since the event do not state which row was changed the following booleans
      // define wether the values are valid or not (needed for multiple row selection)
      setDir := False;
      setCST := False;
      setLen := False;
      checksumType := cstAuto;
      direction := dirSubscriberAutoLength;
      length := 0;

      // Checksum property changed
      value := pgGFTDef.Values[propChecksumType];
      if (value <> EmptyStr) then
      begin
        setCST := True;
        if (value = ChecksumTypeToString(cstCustom)) then
          checksumType := cstCustom
        else if (value = ChecksumTypeToString(cstClassic)) then
          checksumType := cstClassic
        else if (value = ChecksumTypeToString(cstEnhanced)) then
          checksumType := cstEnhanced
        else if (value = ChecksumTypeToString(cstAuto)) then
          checksumType := cstAuto;
      end;

      // Direction property changed
      value := pgGFTDef.Values[propDirection];
      if (value <> EmptyStr) then
      begin
        setDir := True;
        if (value = DirectionToString(dirDisabled)) then
          direction := dirDisabled
        else if (value = DirectionToString(dirPublisher)) then
          direction := dirPublisher
        else if (value = DirectionToString(dirSubscriber)) then
          direction := dirSubscriber
        else if (value = DirectionToString(dirSubscriberAutoLength)) then
          direction := dirSubscriberAutoLength;
      end;

      // Length property changed
      value := pgGFTDef.Values[propLength];
      if (value <> EmptyStr) then
      begin
        if TryStrToInt(value, length) then
          setLen := True;
      end;

      for i := 0 to m_SelectedFrames.Count - 1 do
      begin
        frame := TFrameDefinition(m_SelectedFrames.Items[i]);
        // Update CST: avoid unnecessary updates
        if setCST and (frame.ChecksumType <> checksumType) then
        begin
          // Call for propertyChanged to update dialog controls
          frame.SetChecksumType(checksumType, True);
          // If changes were not validated, rollback/update display
          if (frame.ChecksumType <> checksumType) then
            UpdatePropertyGrid;
        end;
        // Update Direction: avoid unnecessary updates
        if setDir and (frame.Direction <> direction) then
        begin
          // Call for propertyChanged to update dialog controls
          frame.SetDirection(direction, True);
          // If changes were not validated, rollback/update display
          if (frame.Direction <> direction) then
              UpdatePropertyGrid;
        end;
        // Update length: avoid unnecessary updates
        if setLen and (frame.Length <> length) then
        begin
          // Call for propertyChanged to update dialog controls
          frame.SetLength(length, True);
          // If changes were not validated, rollback/update display
          if (frame.Length <> length) then
            UpdatePropertyGrid;
        end;

        // Update the frame in the list view
        lvGFT.Items.BeginUpdate;
        try
          CurrentItem := lvGFT.Items[frame.Id];
          CurrentItem.SubItems[0] := frame.ProtectedIdAsString;
          CurrentItem.SubItems[1] := frame.DirectionAsString;
          CurrentItem.SubItems[2] := frame.LengthAsString;
          CurrentItem.SubItems[3] := frame.ChecksumTypeAsString;
        finally
          lvGFT.Items.EndUpdate;
        end;
      end;
    finally
      // Remove lock
      m_fAutomatic := False;
    end;
  end;
end;

end.
