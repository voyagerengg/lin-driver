unit GlobalFrameTable;

interface

uses
  Classes, PLinApi;

const
  GLOBALFRAMETABLE_SIZE = 64;

  propDirection = 1;
  propChecksumType = 2;
  propLength = 3;

  SLinCSTAuto             = 'Auto';
  SLinCSTClassic          = 'Classic';
  SLinCSTCustom           = 'Custom';
  SLinCSTEnhanced         = 'Enhanced';
  SLinDirectionAuto       = 'Subscriber Automatic Length';
  SLinDirectionDisabled   = 'Disabled';
  SLinDirectionPublisher  = 'Publisher';
  SLinDirectionSubscriber = 'Subscriber';

type
  // Frame property type
  TFrameProperty = Word;

  TGlobalFrameTable = class;

  // LIN Frame Definition class for handling property changes and message communication
  //
  TFrameDefinition = class(TObject)
  private
    // The parent TGlobalFrameTable object containing this frame
    m_parent: TGlobalFrameTable;
    // LIN-Frame Identifier
    m_bID: Byte;
    // Data length of the LIN-Frame
    m_nLength: Integer;
    // Type of the Checksum Calculation
    m_checksumType: TLINChecksumType;
    // Direction of the LIN-Frame
    m_direction: TLINDirection;
    // Protected LIN-Frame Identifier
    m_bProtectedID: Byte;
    // Property getters
    function GetIdAsString: String;
    function GetProtectedIdAsString: String;
    function GetLengthAsString: String;
    function GetChecksumTypeAsString: String;
    function GetDirectionAsString: String;
  public
    constructor Create(
        const AParent: TGlobalFrameTable;       // Parent TGlobalFrameTable object containing this frame
        const AId: Byte;                        // LIN-Frame ID
        const AProtectedId: Byte;               // LIN-Frame Protected ID
        const ALength: Integer;                 // LIN-Frame length
        const AChecksumType: TLINChecksumType;  // LIN-Frame checksum type
        const ADirection: TLINDirection);       // LIN-Frame direction

    // Returns the LIN-Frame identifier
    property Id: Byte read m_bID;
    // Returns the LIN-Frame identifier as a string
    property IdAsString: String read GetIdAsString;

    // Returns the protected LIN-Frame identifier
    property ProtectedId: Byte read m_bProtectedID;
    // Returns the protected LIN-Frame identifier as a string
    property ProtectedIdAsString: String read GetProtectedIdAsString;

    // Returns the LIN-Frame length as an integer
    property Length: Integer read m_nLength;
    // Returns the LIN-Frame length as a string
    property LengthAsString: String read GetLengthAsString;

    // Returns the LIN-Frame checksumType.
    property ChecksumType: TLINChecksumType read m_checksumType;
    // Returns the LIN-Frame checksumType as a string
    property ChecksumTypeAsString: String read GetChecksumTypeAsString;

    // Returns the LIN-Frame direction.
    property Direction: TLINDirection read m_direction;
    // Returns the LIN-Frame direction as string.
    property DirectionAsString: String read GetDirectionAsString;

    // Setters with calls to "CallFramePropertyChanged" parent function

    // Sets the LIN-Frame length
    procedure SetLength(const length: Integer; const doCallFramePropertyChanged: Boolean = False);
    // Sets the LIN-Frame checksumType
    procedure SetChecksumType(const checksumType: TLINChecksumType; const doCallFramePropertyChanged: Boolean = False);
    // Sets the LIN-Frame direction
    procedure SetDirection(const direction: TLINDirection; const doCallFramePropertyChanged: Boolean = False);
  end;

  // LIN Global Frame Table class, contains and handles the 64 LIN Frames/TFrameDefinition
  //
  TGlobalFrameTable = class(TObject)
  private
    // LIN Client handle
    m_hClient: HLINCLIENT;
    // LIN Hardware handle
    m_hHw: HLINHW;
    // LIN Hardware Modus
    m_HwMode: TLINHardwareMode;
    // Pointer to LIN Client filter mask
    m_pMask: ^UInt64;

    // Collection of all frame definitions
    m_FrameDefinitions: array[0..GLOBALFRAMETABLE_SIZE-1] of TFrameDefinition;

    // Getter for Count property
    function GetCount: Integer;

    // Getter for Frames property
    function GetFrames(index: Integer): TFrameDefinition;

    // Function is called when a property of TFrameDefinition is changed to assert the modification is allowed.
    function CallFramePropertyChanged(
        propertyType: TFrameProperty;  // The property to modify
        frame: TFrameDefinition;       // Frame to update
        value: TObject                 // The new value of the property
        ): Boolean;                    // Return value determines whether the modification is allowed or not

    // Adds a new frame definition to the global frame table.
    function AddFrameDefinition(
        id: Byte;                       // Position (and ID) of the frame in the table
        length: Integer;                // Length
        checksumType: TLINChecksumType; // Checksum type
        direction: TLINDirection        // Direction
        ): Integer;                     // Returns the frame index in the table (the same as id unless an error occurred)

  public
    constructor Create;
    destructor Destroy; override;

    // Returns the size of the Frame Table
    property Count: Integer read GetCount;

    // Returns the TFrameDefinition object based on the specified index (with 0 <= index < 64)
    property Frames[Index: Integer]: TFrameDefinition read GetFrames; default;

    // Returns the index in the table for the specified Frame object
    function IndexOf(const frame: TFrameDefinition): Integer;

    // Sets internal LIN informations
    procedure UpdateLinInfo(const hClient: HLINCLIENT; const hHw: HLINHW; const hwMode: TLINHardwareMode;
      const pMask: Pointer);
  end;


// Helper functions

// Converts a LIN Frame checksum type into a human-readable string
function ChecksumTypeToString(const checksumType: TLINChecksumType): String;

// Converts a LIN Frame Direction into a human-readable string
function DirectionToString(const direction: TLINDirection): String;


implementation

uses
  SysUtils;


//////////////////////////////////////////////////////////////////////////////////////////////
// Helper functions

function ChecksumTypeToString(const checksumType: TLINChecksumType): String;
begin
  case checksumType of
    cstCustom:
      Result := SLinCSTCustom;
    cstClassic:
      Result := SLinCSTClassic;
    cstEnhanced:
      Result := SLinCSTEnhanced;
    cstAuto:
      Result := SLinCSTAuto;
    else
      Result := EmptyStr;
  end;
end;

function DirectionToString(const direction: TLINDirection): String;
begin
  case direction of
    dirDisabled:
      Result := SLinDirectionDisabled;
    dirPublisher:
      Result := SLinDirectionPublisher;
    dirSubscriber:
      Result := SLinDirectionSubscriber;
    dirSubscriberAutoLength:
      Result := SLinDirectionAuto;
    else
      Result := EmptyStr;
  end;
end;


//////////////////////////////////////////////////////////////////////////////////////////////
// TFrameDefinition class

constructor TFrameDefinition.Create(
    const AParent: TGlobalFrameTable;       // Global Frame Table holding this frame
    const AId: Byte;                        // LIN-Frame ID
    const AProtectedId: Byte;               // LIN-Frame Protected ID
    const ALength: Integer;                 // LIN-Frame length
    const AChecksumType: TLINChecksumType;  // LIN-Frame checksum type
    const ADirection: TLINDirection);
begin
  m_parent := AParent;
  m_bId := AId;
  m_bProtectedID := AProtectedId;
  m_nLength := ALength;
  m_checksumType := AChecksumType;
  m_direction := ADirection;
end;

function TFrameDefinition.GetIdAsString: String;
begin
  Result := Format('%.02Xh', [m_bID]);
end;

function TFrameDefinition.GetProtectedIdAsString: String;
begin
  Result := Format('%.02Xh', [m_bProtectedID]);
end;

function TFrameDefinition.GetLengthAsString: String;
begin
  Result := IntToStr(m_nLength);
end;

function TFrameDefinition.GetChecksumTypeAsString: String;
begin
  Result := ChecksumTypeToString(m_ChecksumType);
end;

function TFrameDefinition.GetDirectionAsString: String;
begin
  Result := DirectionToString(m_direction);
end;

procedure TFrameDefinition.SetLength(const length: Integer; const doCallFramePropertyChanged: Boolean);
var
  allowed: Boolean;
begin
  allowed := True;
  if ((m_parent <> nil) and doCallFramePropertyChanged) then
    allowed := m_parent.CallFramePropertyChanged(propLength, Self, TObject(length));
  if allowed then
    m_nLength := length;
end;

procedure TFrameDefinition.SetChecksumType(const checksumType: TLINChecksumType; const doCallFramePropertyChanged: Boolean);
var
  allowed: Boolean;
begin
  allowed := True;
  if ((m_parent <> nil) and doCallFramePropertyChanged) then
    allowed := m_parent.CallFramePropertyChanged(propChecksumType, Self, TObject(checksumType));
  if allowed then
    m_checksumType := checksumType;
end;

procedure TFrameDefinition.SetDirection(const direction: TLINDirection; const doCallFramePropertyChanged: Boolean);
var
  allowed: Boolean;
begin
  allowed := True;
  if ((m_parent <> nil) and (doCallFramePropertyChanged)) then
    allowed := m_parent.CallFramePropertyChanged(propDirection, Self, TObject(direction));
  if allowed then
    m_direction := direction;
end;


//////////////////////////////////////////////////////////////////////////////////////////////
// TGlobalFrameTable class

constructor TGlobalFrameTable.Create;
var
  id: Byte;
begin
  m_hClient := 0;
  m_hHw := 0;
  m_HwMode := modNone;
  m_pMask := nil;

  // Create all TFrameDefinition objects and set their values to default.
  // The length values is set to LIN 1.2.
  // Default values:
  // - Direction = SubscriberAutoLength
  // - ChecksumType = Auto
  // - Length = see inside the loop
  for id := 0 to GLOBALFRAMETABLE_SIZE - 1 do
  begin
    // From Frame-ID 0 to 31 set the length 2
    if (id <= $1F) then
      AddFrameDefinition(id, 2, cstAuto, dirSubscriberAutoLength)
    // From Frame-ID 32 to 47 set the length 4
    else  if ((id >= $20) and (id <= $2F)) then
      AddFrameDefinition(id, 4, cstAuto, dirSubscriberAutoLength)
    // From Frame-ID 48 to 63 set the length 8
    else if ((id >= $30) and (id <= $3F)) then
      AddFrameDefinition(id, 8, cstAuto, dirSubscriberAutoLength);
  end;
end;

destructor TGlobalFrameTable.Destroy;
var
  i: Integer;
begin
  // Delete all frames
  for i := 0 to Count - 1 do
    m_FrameDefinitions[i].Free;
  inherited Destroy;
end;

function TGlobalFrameTable.CallFramePropertyChanged(
  propertyType: TFrameProperty;  // The property to modify
  frame: TFrameDefinition;       // Frame to update
  value: TObject                 // The new value of the property
  ): Boolean;                    // Return value determines whether the modification is allowed or not
var
  frameEntry: TLINFrameEntry;
  err: TLINError;
  mask: UInt64;
begin
  Result := True;
  if (frame <> nil) then
  begin
    // If data length is to be set, check the value
    if (propertyType = propLength) then
      // Only values between 0 and 8 are valid
      Result := ((Integer(value) >= 0) and (Integer(value) <= 8));
    // If not allowed then return
    if not Result then
      Exit;

    // Set the direction of the LIN-Frame only if the hardware was initialized as
    // Slave; the Master uses the direction with LIN_Write
    if (m_HwMode = modSlave) then
    begin
      // Set the Frame-ID of the frame to get and set.
      // The ID has to be set before getting the entry
      frameEntry.FrameId := frame.Id;
      // Get the frame entry with the Frame-ID from the Hardware via the LIN-API
      err := LIN_GetFrameEntry(m_hHw, frameEntry);
      // If an error occurs do not allow to change the property and return
      if (err <> errOK) then
      begin
        Result := False;
        Exit;
      end;
      // Switch between the different kind of property types
      case (propertyType) of
        // Direction property should be set
        propDirection:
          frameEntry.Direction := TLINDirection(value);
        // Length property should be set
        propLength:
          frameEntry.Length := Byte(value);
        // ChecksumType property should be set
        propChecksumType:
          frameEntry.ChecksumType := TLINChecksumType(value);
      end;

      frameEntry.Flags := FRAME_FLAG_RESPONSE_ENABLE;
      err := LIN_SetFrameEntry(m_hClient, m_hHw, frameEntry);
      // If an error occurs do not allow to change the property and return
      if (err <> errOK) then
      begin
        Result := False;
        Exit;
      end;
    end;

    // If the property 'Direction' of one TFrameDefinition object is changed,
    // we need a special request to set the client filter here
    if (propertyType = propDirection) then
    begin
      // If the new value for the property 'Direction' is not 'Disabled', then
      // check if the TFrameDefinition is defined already with some
      // other value than 'Disabled'
      if (TLINDirection(value) <> dirDisabled) then
      begin
        if (frame.Direction = dirDisabled) then
        begin
          // If the old direction of TFrameDefinition was set to 'Disabled', the
          // new value means that the Frame-ID has to be added to the client filter
          // via the LIN-API.
          // Set the client filter, the filter value is a bit mask
          mask := UInt64(1) shl frame.Id;
          m_pMask^ := (m_pMask^ or mask);
          err := LIN_SetClientFilter(m_hClient, m_hHw, UInt64(m_pMask^));
          // Only allow to change the property if the Frame-ID has been successfully
          // added to the filter
          Result := err = errOK;
        end;
      end
      else
      begin
        // If the direction is set to 'Disabled', remove the Frame-ID from the
        // client filter
        mask := UInt64(1) shl frame.Id;
        m_pMask^ := (m_pMask^ and not mask);
        err := LIN_SetClientFilter(m_hClient, m_hHw, UInt64(m_pMask^));
        // Only allow to change the property if the Frame-ID has been removed
        // successfully from the filter
        Result := err = errOK;
      end;
    end;
  end;
end;

function TGlobalFrameTable.AddFrameDefinition(
  id: Byte;                        // Position (and ID) of the frame in the table
  length: Integer;                 // Length
  checksumType: TLINChecksumType;  // Checksum type
  direction: TLINDirection         // Direction
  ): Integer;                      // Returns the frame index in the table (the same as id unless an error occurred)
var
  protectedId: Byte;
  frame: TFrameDefinition;
begin
  // Check the Frame-ID before adding it, only ID's from 0 to 63 are allowed
  if (id > 63) then
    Result := -1    // ID is invalid, do not add it
  else
  begin
    // The specified Frame-ID is valid.
    // Calculate the Protected-ID from the specified ID
    protectedId := id;
    LIN_GetPID(protectedId);
    // Create a TFrameDefinition object and assign the specified values to it
    frame := TFrameDefinition.Create(Self, id, protectedId, length, checksumType, direction);
    // Add the created object to the list and return the position of the newly added object.
    // It should be added at the end so the position must be the last entry in the list.
    m_FrameDefinitions[id] := frame;
    Result := id;
  end;
end;

function TGlobalFrameTable.GetCount;
begin
  Result := Length(m_FrameDefinitions);
end;

function TGlobalFrameTable.GetFrames(Index: Integer): TFrameDefinition;
begin
  if (Index >= 0) and (Index < Count) then
    Result := m_FrameDefinitions[Index]
  else
    Result := nil;
end;

function TGlobalFrameTable.IndexOf(const frame: TFrameDefinition): Integer;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    if (m_FrameDefinitions[i] = frame) then
    begin
      Result := i;
      Exit;
    end;
  Result := -1;
end;

procedure TGlobalFrameTable.UpdateLinInfo(const hClient: HLINCLIENT; const hHw: HLINHW; const hwMode: TLINHardwareMode;
  const pMask: Pointer);
begin
  // Up-to-date LIN information is required by function CallFramePropertyChanged
  m_hClient := hClient;
  m_hHw := hHw;
  m_HwMode := hwMode;
  m_pMask := pMask;
end;

end.
