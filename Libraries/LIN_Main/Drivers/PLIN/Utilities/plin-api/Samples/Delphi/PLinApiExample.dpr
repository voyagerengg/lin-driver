program PLinApiExample;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  PLinApi in 'PLinApi.pas',
  GlobalFrameTable in 'GlobalFrameTable.pas' {,
  Frame_Dlg in 'Frame_Dlg.pas' {FrameDlg},
  FrameDlg in 'FrameDlg.pas' {GlobalFrameTableDialog};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
