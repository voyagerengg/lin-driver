﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="LIN_Main Module" Type="Folder">
			<Item Name="Drivers" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Interface" Type="Folder">
					<Item Name="Generic LIN INFC.lvclass" Type="LVClass" URL="../Libraries/LIN_Main/Drivers/Generic_LIN/Generic LIN INFC.lvclass"/>
				</Item>
				<Item Name="PLIN" Type="Folder">
					<Item Name="PLIN.lvclass" Type="LVClass" URL="../Libraries/LIN_Main/Drivers/PLIN/PLIN.lvclass"/>
				</Item>
				<Item Name="NI XNET" Type="Folder">
					<Item Name="NI_XNET_LIN.lvclass" Type="LVClass" URL="../Libraries/LIN_Main/Drivers/NI LIN XNET/NI_XNET_LIN.lvclass"/>
				</Item>
			</Item>
			<Item Name="LIN_Main.lvlib" Type="Library" URL="../Libraries/LIN_Main/LIN_Main.lvlib"/>
		</Item>
		<Item Name="Support" Type="Folder">
			<Item Name="kdi_labview_api_for_plin-1.0.0.3.vip" Type="Document" URL="../Libraries/LIN_Main/Drivers/PLIN/Dependencies/kdi_labview_api_for_plin-1.0.0.3.vip"/>
			<Item Name="PLinApi.dll" Type="Document" URL="../Libraries/LIN_Main/Drivers/PLIN/Dependencies/PLinApi.dll"/>
			<Item Name="ReadMe.txt" Type="Document" URL="../Libraries/LIN_Main/Drivers/PLIN/Dependencies/ReadMe.txt"/>
		</Item>
		<Item Name="Test LIN_Main API.vi" Type="VI" URL="../Libraries/LIN_Main/Test LIN_Main API.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="2_x_U8_littel_Endian_to_U16.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Converter_VIs/2_x_U8_littel_Endian_to_U16.vi"/>
				<Item Name="4_x_U8_littel_Endian_to_U32.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Converter_VIs/4_x_U8_littel_Endian_to_U32.vi"/>
				<Item Name="Baudrate.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/Baudrate.ctl"/>
				<Item Name="Checksum.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/Checksum.ctl"/>
				<Item Name="Client_Handle.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/Client_Handle.ctl"/>
				<Item Name="Conv_U8_array_to_8byte_long_U8_array.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Converter_VIs/Conv_U8_array_to_8byte_long_U8_array.vi"/>
				<Item Name="Data.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/Data.ctl"/>
				<Item Name="Flags.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/Flags.ctl"/>
				<Item Name="FrameId.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/FrameId.ctl"/>
				<Item Name="Get hWnd.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Get hWnd.vi"/>
				<Item Name="Hardware_Handle.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/Hardware_Handle.ctl"/>
				<Item Name="InitialData.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/InitialData.ctl"/>
				<Item Name="LIN_ConnectClient.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_ConnectClient.vi"/>
				<Item Name="LIN_DisconnectClient.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_DisconnectClient.vi"/>
				<Item Name="LIN_GetPID.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Get_Functions/LIN_GetPID.vi"/>
				<Item Name="LIN_InitializeHardware .vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_InitializeHardware .vi"/>
				<Item Name="LIN_MAX_NAME_LENGTH.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/const/LIN_MAX_NAME_LENGTH.ctl"/>
				<Item Name="LIN_Read_Multi.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_Read_Multi.vi"/>
				<Item Name="LIN_RegisterClient.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_RegisterClient.vi"/>
				<Item Name="LIN_RegisterFrameId.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_RegisterFrameId.vi"/>
				<Item Name="LIN_RemoveClient.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_RemoveClient.vi"/>
				<Item Name="LIN_ResetHardware.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_ResetHardware.vi"/>
				<Item Name="LIN_SetClientFilter.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Set_Functions/LIN_SetClientFilter.vi"/>
				<Item Name="LIN_SetFrameEntry.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Set_Functions/LIN_SetFrameEntry.vi"/>
				<Item Name="LIN_UpdateByteArray.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_UpdateByteArray.vi"/>
				<Item Name="LIN_Write.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/LIN_Write.vi"/>
				<Item Name="PLinApi.dll" Type="Document" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/PLinApi.dll"/>
				<Item Name="TimeStamp.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/TimeStamp.ctl"/>
				<Item Name="TLINChecksumType.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/enum/TLINChecksumType.ctl"/>
				<Item Name="TLINDirection.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/enum/TLINDirection.ctl"/>
				<Item Name="TLINError.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Decode_VIs/TLINError.vi"/>
				<Item Name="TLINFrameEntry.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/structures_clusters/TLINFrameEntry.ctl"/>
				<Item Name="TLINHardwareMode.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/enum/TLINHardwareMode.ctl"/>
				<Item Name="TLINMsgErrors.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/variables/TLINMsgErrors.ctl"/>
				<Item Name="TLINMsgErrors_RCV.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Decode_VIs/TLINMsgErrors_RCV.vi"/>
				<Item Name="TLINMsgType.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/enum/TLINMsgType.ctl"/>
				<Item Name="TLINRcvMsg.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/structures_clusters/TLINRcvMsg.ctl"/>
				<Item Name="TLINSendMsg.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/structures_clusters/TLINSendMsg.ctl"/>
				<Item Name="U16_to_littel_Endian_U8_byte_array.vi" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/VIs/Converter_VIs/U16_to_littel_Endian_U8_byte_array.vi"/>
				<Item Name="Update_Byte_Array_Data.ctl" Type="VI" URL="/&lt;instrlib&gt;/KDI Digital Instrumentation/KDI_LabVIEW_API_for_PLIN/var/structures_clusters/Update_Byte_Array_Data.ctl"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create New Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="Delacor_lib_QMH_Message Queue.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Message Queue_class/Delacor_lib_QMH_Message Queue.lvclass"/>
				<Item Name="Delacor_lib_QMH_Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Module Admin_class/Delacor_lib_QMH_Module Admin.lvclass"/>
				<Item Name="Destroy A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy A Rendezvous.vi"/>
				<Item Name="Destroy Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="FormatTime String.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/FormatTime String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Not A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Release Waiting Procs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
				<Item Name="RemoveNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RemoveNamedRendezvousPrefix.vi"/>
				<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
				<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subElapsedTime.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/subElapsedTime.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
